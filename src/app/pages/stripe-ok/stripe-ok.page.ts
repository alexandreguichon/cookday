import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import firebase from 'firebase/app';

@Component({
    selector: 'app-stripe-ok',
    templateUrl: './stripe-ok.page.html',
    styleUrls: ['./stripe-ok.page.scss'],
})
export class StripeOkPage implements OnInit {

    db = firebase.firestore();

    constructor(
        private route: ActivatedRoute,
        private router: Router
    ) {
        let self = this;
        this.route.params.subscribe(params => {
            if (params && params['id']) {
                setTimeout(function () {
                    self.db.collection('cuisines').doc(params.id).update({
                        stripeValidated: true
                    })
                }, 1000);
            }
        });
    }

    ngOnInit() {
    }

}
