import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { StripeOkPageRoutingModule } from './stripe-ok-routing.module';

import { StripeOkPage } from './stripe-ok.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    StripeOkPageRoutingModule
  ],
  declarations: [StripeOkPage]
})
export class StripeOkPageModule {}
