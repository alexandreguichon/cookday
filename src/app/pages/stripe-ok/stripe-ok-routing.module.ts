import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { StripeOkPage } from './stripe-ok.page';

const routes: Routes = [
  {
    path: '',
    component: StripeOkPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class StripeOkPageRoutingModule {}
