import {Component, OnInit} from '@angular/core';
import {NavController} from "@ionic/angular";
import {VentesService} from "../../services/ventes.service";
import {UserService} from "../../services/user.service";
import firebase from 'firebase/app';
import {FcmService} from "../../services/fcm.service";

@Component({
    selector: 'app-finaliser-commande',
    templateUrl: './finaliser-commande.page.html',
    styleUrls: ['./finaliser-commande.page.scss'],
})
export class FinaliserCommandePage implements OnInit {

    db = firebase.firestore();

    cardnumber = document.getElementById('cardnumber');

    carte = {
        numero: null,
        dateExpiration: null,
        cvc: null
    }

    heureRecup: any = this.toISOString(this.minus1Hour(this.ventesService.ventes[this.userService.user.panier.vente].horaires.debut));

    constructor(
        private navController: NavController,
        public ventesService: VentesService,
        public userService: UserService,
        private fcmService: FcmService,
    ) {
    }

    ngOnInit() {
    }

    ionViewDidEnter() {
    }

    makeDate(date: any) {
        return date.seconds * 1000;
    }

    minus1Hour(date: any) {
        let res = date;
        res.setHours(date.getHours() - 1);
        return res;
    }

    async back() {
        return this.navController.pop();
    }

    disabledPayer() {
        return !(this.carte.numero && this.carte.dateExpiration && this.carte.cvc)
            || this.carte.numero.length < 19
            || this.carte.dateExpiration.length < 5
            || this.carte.cvc.length < 3;
    }

    // finaliserCommande() {
    //     this.userService.presentLoading("Commande en cours").then(() => {
    //         let dateRecup = new Date(this.ventesService.ventes[this.userService.user.panier.vente].date);
    //         let hours = new Date(this.heureRecup);
    //         dateRecup.setHours(hours.getHours(), hours.getMinutes());
    //         let newCommande = {
    //             ajoutCouvertsServiettes: this.userService.user.panier.ajoutCouvertsServiettes,
    //             cuisine: this.userService.user.panier.cuisine,
    //             dateRecup: new Date(dateRecup),
    //             dateRes: new Date(),
    //             menu: this.userService.user.panier.menu,
    //             statut: "à préparer",
    //             user: this.userService.userInfo.uid,
    //             vente: this.userService.user.panier.vente,
    //         }
    //
    //         this.db.collection("commandes").add(newCommande).then(res => {
    //             this.db.collection("commandes").doc(res.id).update({id: res.id}).then(() => {
    //                 this.userService.user.commandes.push(res.id);
    //                 newCommande['id'] = res.id;
    //                 this.userService.commandesData[res.id] = newCommande;
    //
    //                 this.ventesService.order.forEach(cat => {
    //                     this.userService.user.panier.menu[this.ventesService.retirerAccents(cat)].forEach(plat => {
    //                         this.ventesService.ventes[this.userService.user.panier.vente].menu[this.ventesService.retirerAccents(cat)].find(e => e.nom == plat.nom && e.prix == plat.prix).qteAchetee += plat.quantite;
    //                     })
    //                 })
    //
    //                 this.ventesService.ventes[this.userService.user.panier.vente].commandes.push(res.id);
    //
    //                 let newNotif = {
    //                     cuisine: this.userService.user.panier.cuisine,
    //                     user: this.userService.userInfo.uid,
    //                     date: new Date(),
    //                     text: this.userService.userInfo.prenom + " a passé une commande.",
    //                     action: "commandeRecue",
    //                     objectId: this.userService.userInfo.uid
    //                 }
    //
    //                 Promise.all([
    //                     this.db.collection("ventes").doc(this.userService.user.panier.vente).update({
    //                         commandes: this.ventesService.ventes[this.userService.user.panier.vente].commandes
    //                     }),
    //                     this.db.collection("utilisateurs").doc(this.userService.userInfo.uid).update({
    //                         commandes: this.userService.user.commandes,
    //                         panier: null
    //                     }),
    //                     this.db.collection('utilisateurs').doc(this.userService.user.panier.cuisine).update({
    //                         notifs: firebase.firestore.FieldValue.arrayUnion(newNotif)
    //                     }),
    //                     this.fcmService.sendPostRequest(
    //                         'Nouvelle commande',
    //                         this.userService.user.prenom + " a passé une commande.",
    //                         this.userService.user.photo ? this.userService.user.photo : "assets/addPhoto.jpg",
    //                         newCommande,
    //                         "",
    //                         this.userService.userInfo.uid,
    //                         this.ventesService.getCuisineById(this.userService.user.panier.cuisine).token,
    //                         this.userService.user.prenom + " a passé une commande.",
    //                         "commandeRecue",
    //                        "")
    //                 ]).then(() => {
    //                     this.back().then(() => {
    //                         this.userService.user.panier = null;
    //                         this.userService.loadingController.dismiss();
    //                         this.userService.toast("La commande a été passée.", 2000);
    //                     });
    //                 })
    //             })
    //         })
    //     })
    // }

    changeHeureRecup($event) {
        this.heureRecup = $event.detail.value;
    }

    toISOString(date: any) {
        let newDate = new Date(date);
        newDate.setHours(newDate.getHours() + 1);
        return newDate.toISOString();
    }

}
