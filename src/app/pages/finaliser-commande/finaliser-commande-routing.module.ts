import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FinaliserCommandePage } from './finaliser-commande.page';

const routes: Routes = [
  {
    path: '',
    component: FinaliserCommandePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FinaliserCommandePageRoutingModule {}
