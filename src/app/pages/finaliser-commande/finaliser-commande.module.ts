import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';

import {IonicModule} from '@ionic/angular';

import {FinaliserCommandePageRoutingModule} from './finaliser-commande-routing.module';

import {FinaliserCommandePage} from './finaliser-commande.page';

import {StripePaymentComponent} from "../../components/stripe/stripe-payment/stripe-payment.component";
import {NgxStripeModule} from "ngx-stripe";
import {environment} from 'src/environments/environment';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    FinaliserCommandePageRoutingModule,
    NgxStripeModule.forRoot(environment.stripeKey)
  ],
  declarations: [FinaliserCommandePage, StripePaymentComponent]
})
export class FinaliserCommandePageModule {}
