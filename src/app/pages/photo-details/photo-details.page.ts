import {ChangeDetectorRef, Component, OnInit, ViewRef} from '@angular/core';
import {NavController} from "@ionic/angular";
import {ActivatedRoute} from "@angular/router";

@Component({
    selector: 'app-photo-details',
    templateUrl: './photo-details.page.html',
    styleUrls: ['./photo-details.page.scss'],
})
export class PhotoDetailsPage implements OnInit {

    imageOk = false;
    src = "";

    constructor(
        private cd: ChangeDetectorRef,
        public navCtrl: NavController,
        private route: ActivatedRoute
    ) {
        this.route.queryParams.subscribe(params => {
            if (params && params.img) {
                this.src = JSON.parse(params.img);
            }
        });
    }

    ngOnInit() {
    }

    back() {
        this.navCtrl.pop();
    }

    detectChanges() {
        setTimeout(() => {
            if (this.cd && !(this.cd as ViewRef).destroyed) {
                this.cd.detectChanges();
            }
        });
    }

    getImageVisibility() {
        if (this.imageOk) {
            return 'visible';
        } else {
            return 'hidden';
        }
    }

    imageChargee() {
        this.imageOk = true;
        this.detectChanges();
    }

}
