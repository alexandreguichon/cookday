import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { StripeAccountRedirectPage } from './stripe-account-redirect.page';

const routes: Routes = [
  {
    path: '',
    component: StripeAccountRedirectPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class StripeAccountRedirectPageRoutingModule {}
