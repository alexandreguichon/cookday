import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { StripeAccountRedirectPageRoutingModule } from './stripe-account-redirect-routing.module';

import { StripeAccountRedirectPage } from './stripe-account-redirect.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    StripeAccountRedirectPageRoutingModule
  ],
  declarations: [StripeAccountRedirectPage]
})
export class StripeAccountRedirectPageModule {}
