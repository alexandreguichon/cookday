import {Component, OnInit} from '@angular/core';
import {StripeHttpService} from "../../services/stripe-http.service";
import {UserService} from "../../services/user.service";
import {ActivatedRoute, NavigationExtras, Router} from "@angular/router";
import firebase from 'firebase/app';
import {fromPromise} from "rxjs/internal-compatibility";
import {map, switchMap, tap} from "rxjs/operators";

@Component({
    selector: 'app-stripe-account-redirect',
    templateUrl: './stripe-account-redirect.page.html',
    styleUrls: ['./stripe-account-redirect.page.scss'],
})
export class StripeAccountRedirectPage implements OnInit {

    id = "";
    validated: string = "";

    constructor(private readonly stripeHttpService: StripeHttpService,
                private readonly userService: UserService,
                private readonly router: Router,
                private route: ActivatedRoute,
    ) {
        this.route.queryParams.subscribe(params => {
            if (params && params.id) {
                this.id = params.id;
                this.initActions();
            }
        });
    }

    ngOnInit() {

    }

    initActions() {
        const db = firebase.firestore();
        fromPromise(db.collection('cuisines').doc(this.id).get()).pipe(
            map((doc) => doc.data().stripeAccountId),
            switchMap((accountId) => this.stripeHttpService.isActiveAccount(accountId)),
            tap((active) => {
                if (active) {
                    this.validated = "yes";
                    this.router.navigate(['principale'])
                        .then(() => this.userService.loadingController.dismiss())
                        .then(() => this.userService.toast('Félicitations! Aux fourneaux!', 2000));
                } else {
                    this.validated = "no"
                }
            })
        ).subscribe();
    }

    goPrincipale() {
        this.router.navigate(['principale']);
    }

    goStripe() {
        let navigationExtras: NavigationExtras = {
            replaceUrl: true,
            queryParams: {
                id: this.userService.userInfo.uid
            }
        };
        this.router.navigate(['stripe-account'], navigationExtras);
    }

}
