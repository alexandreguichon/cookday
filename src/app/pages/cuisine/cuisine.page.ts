import {ChangeDetectorRef, Component, OnInit, ViewChild, ViewRef} from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {VentesService} from "../../services/ventes.service";
import {IonSlides, NavController} from "@ionic/angular";
import {UserService} from "../../services/user.service";
import firebase from 'firebase/app';

@Component({
    selector: 'app-cuisine',
    templateUrl: './cuisine.page.html',
    styleUrls: ['./cuisine.page.scss'],
})
export class CuisinePage implements OnInit {

    id: string;
    segment: string = "0";
    @ViewChild('slides', {static: true}) slides: IonSlides;
    db = firebase.firestore();

    slideOpts = {
        effect: 'cube',
        autoHeight: true,
    };

    segments = ["Détails", "Plats", "Évaluations"];

    order = ['entrees', 'plats', 'desserts', 'boissons'];

    constructor(
        private route: ActivatedRoute,
        public ventesService: VentesService,
        private navController: NavController,
        public userService: UserService,
        private cd: ChangeDetectorRef,
    ) {
        this.route.queryParams.subscribe(params => {
            if (params && params.id) {
                this.id = JSON.parse(params.id);
            }
        });
    }

    ngOnInit() {
        this.cuisine().avis.forEach(avis => {
            if(!avis.userData){
                this.db.collection('utilisateurs').doc(avis.user).get().then(user => {
                    avis.userData = user.data();
                    console.log(user.id);
                })
            }
        })
    }

    cuisine(){
        return this.ventesService.cuisines[this.id];
    }

    back(){
        this.navController.pop();
    }

    onSlideChange(event: any) {
        let self = this;
        event.then(function (result) {
            self.segment = result.toString();
            self.detectChanges();
        });
    }

    detectChanges() {
        setTimeout(() => {
            if (this.cd && !(this.cd as ViewRef).destroyed) {
                this.cd.detectChanges();
            }
        });
    }

    Number(segment: string) {
        return +segment;
    }

    getBold(commande){
       return commande.statut == "terminée" ? 'unset' : 'bold';
    }

    commandeItemBorder(i){
        return i>=this.userService.getCommandes(this.cuisine()).length-1 ? 'unset' : '1px solid var(--ion-color-medium)';
    }

    venteItemBorder(i){
        return i>=this.ventesService.getVentesCuisine(this.cuisine()).length-1 ? 'unset' : '1px solid var(--ion-color-medium)';
    }

    articleItemBorder(cat, i){
        return i>=this.cuisine().menu[this.ventesService.retirerAccents(cat)].length-1 ? 'unset' : '1px solid var(--ion-color-medium)';
    }

    openEvaluations(){
        this.slides.slideTo(2);
    }

    tempsDepuis(date) {
        let maintenant = new Date();
        let res = "";
        var minutesAgo = Math.round((maintenant.getTime() - date.getTime()) / 60000);
        if (minutesAgo < 60) {
            res = minutesAgo + " minute";
            return minutesAgo==1 ? res : res+"s";
        } else {
            var hoursAgo = Math.round(minutesAgo / 60);
            if (hoursAgo < 24) {
                res = hoursAgo + " heure";
                return hoursAgo==1 ? res : res+"s";
            } else {
                var daysAgo = Math.round(hoursAgo / 24);
                if (daysAgo < 30) {
                    res = daysAgo + " jour";
                    return daysAgo==1 ? res : res+"s";
                } else {
                    var monthsAgo = Math.round(daysAgo / 30);
                    if (monthsAgo < 12) {
                        return monthsAgo + " mois";
                    } else {
                        res = Math.round(monthsAgo / 12) + " année";
                        return Math.round(monthsAgo / 12)==1 ? res : res+"s";
                    }
                }
            }
        }
    }

    toDate(date){
        return date.toDate();
    }

}
