import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CuisinePageRoutingModule } from './cuisine-routing.module';

import { CuisinePage } from './cuisine.page';
import {PrincipalePageModule} from "../principale/principale.module";

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        CuisinePageRoutingModule,
        PrincipalePageModule
    ],
  declarations: [CuisinePage]
})
export class CuisinePageModule {}
