import {ChangeDetectorRef, Component, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import firebase from 'firebase/app';
import {UserService} from "../../services/user.service";
import {IonContent, NavController, Platform} from "@ionic/angular";
import {LoadingController} from "@ionic/angular";
import {Keyboard} from "@capacitor/keyboard";
import {AngularFireDatabase} from "@angular/fire/database";
import {VentesService} from "../../services/ventes.service";
import {FcmService} from "../../services/fcm.service";

@Component({
    selector: 'app-chat',
    templateUrl: './chat.page.html',
    styleUrls: ['./chat.page.scss'],
})
export class ChatPage implements OnInit {

    @ViewChild(IonContent) content: IonContent;

    userId: string;
    cuisineId: string;
    ref: any;
    chatData = [];
    messageContent: string = '';

    constructor(
        private route: ActivatedRoute,
        public userService: UserService,
        public cd: ChangeDetectorRef,
        private loadingController: LoadingController,
        public afDB: AngularFireDatabase,
        private platform: Platform,
        private ventesService: VentesService,
        private navController: NavController,
        private fcmService: FcmService
    ) {
        this.route.queryParams.subscribe(params => {
            if (params && params.userId) {
                this.userId = JSON.parse(params.userId);
            }
            if (params && params.cuisineId) {
                this.cuisineId = JSON.parse(params.cuisineId);
            }
        });
    }

    ngOnInit() {
        this.presentLoading("Chargement en cours").then(() => {
            this.ref = firebase.database().ref('chat/' + this.cuisineId + '/' + this.userId + '/');
            this.ref.on('value', resp => {
                    this.chatData = this.snapshotToArray(resp);
                    this.chatData.filter(message => (message.auteur != this.userService.userInfo.uid) && !message.lu).forEach(message => {
                        firebase.database().ref('chat/' + this.cuisineId + '/' + this.userId + '/' + message.key + '/').update({
                            lu: true
                        })
                    });
                    this.loadingController.dismiss();
                    setTimeout(() => {
                        this.updateScroll();
                    }, 200);
                    this.cd.detectChanges();
                }
            );
        })
    }

    snapshotToArray(snapshot: any) {
        let returnArray = [];
        snapshot.forEach(element => {
            let item = element.val();
            item.key = element.key;
            item.date = new Date(item.date);
            returnArray.push(item);
        });
        return returnArray;
    }

    updateScroll() {
        if (this.content.scrollToBottom) {
            this.content.scrollToBottom(400);
        }
    }

    async presentLoading(message) {
        const loading = await this.loadingController.create({
            message: message
        });
        await loading.present();
    }

    sendMessage() {
        let newMessage = this.messageContent;
        this.messageContent = "";
        if (newMessage.replace(/(\r\n|\n|\r|\s)/gm, "").length > 0) {
            let newDate = new Date();
            this.chatData.push({
                auteur: this.userService.userInfo.uid,
                message: newMessage,
                date: newDate,
                lu: false
            });
            if (this.platform.is('capacitor')) {
                Keyboard.hide();
            }
            this.afDB.list('chat/' + this.cuisineId + '/' + this.userId + '/').push({
                auteur: this.userService.userInfo.uid,
                message: newMessage,
                date: newDate.toString(),
                lu: false
            }).then(res => {
                this.userService.db.collection('utilisateurs').doc(this.cuisineId == this.userService.userInfo.uid ? this.userId : this.cuisineId).update({
                    notifs: firebase.firestore.FieldValue.arrayUnion({
                        cuisine: this.cuisineId,
                        user: this.userId,
                        date: new Date(),
                        text: "Message de " + (this.cuisineId == this.userService.userInfo.uid ? this.user().nom : this.cuisine().nom) + " : " + newMessage,
                        action: "message",
                        objectId: res.key
                    })
                }).then(() => this.fcmService.sendChatNotify(
                    newMessage,
                    res.key,
                    this.cuisineId,
                    this.userId)
                ).then(() => {
                    setTimeout(() => {
                        this.updateScroll();
                    }, 0);
                })
            })
        }
    }

    testaffichDate(i: number, message: any) {
        if (this.chatData[i - 1] && message) {
            let durationBetweenMessages = message.date.getTime() - this.chatData[i - 1].date.getTime();
            let oneHourBetween = durationBetweenMessages >= (60 * 60 * 1000);
            return (this.chatData[i - 1].auteur != message.auteur) || oneHourBetween;
        } else return i == 0;
    }

    testaffichImg(i: number, message: any) {
        if (this.chatData[i + 1] && message) {
            let durationBetweenMessages = this.chatData[i + 1].date.getTime() - message.date.getTime();
            let oneHourBetween = durationBetweenMessages >= (60 * 60 * 1000);
            return (this.chatData[i + 1].auteur != message.auteur) || oneHourBetween;
        } else return i == this.chatData.length - 1;
    }

    cuisine() {
        return this.ventesService.getCuisineById(this.cuisineId);
    }

    user() {
        return this.userService.getUserById(this.userId);
    }

    back() {
        this.navController.pop();
    }

    onAutoFormatChanged() {
        this.messageContent = this.setFirstLetterToUppercase(this.messageContent);
        this.cd.detectChanges();
    }

    setFirstLetterToUppercase(string:string):string {
        return string.charAt(0).toUpperCase() + string.slice(1);
    }

}
