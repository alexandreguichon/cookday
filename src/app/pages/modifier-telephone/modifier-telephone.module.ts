import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ModifierTelephonePageRoutingModule } from './modifier-telephone-routing.module';

import { ModifierTelephonePage } from './modifier-telephone.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ModifierTelephonePageRoutingModule
  ],
  declarations: [ModifierTelephonePage]
})
export class ModifierTelephonePageModule {}
