import {Component, OnInit} from '@angular/core';
import {NavController, Platform, PopoverController} from "@ionic/angular";
import {SelectCountryComponent} from "../../components/select-country/select-country.component";
import {CountriesService} from "../../services/countries.service";
import {NavigationExtras, Router} from "@angular/router";
import {UserService} from "../../services/user.service";
import {AngularFireAuth} from "@angular/fire/auth";
import firebase from 'firebase/app';

@Component({
    selector: 'app-modifier-telephone',
    templateUrl: './modifier-telephone.page.html',
    styleUrls: ['./modifier-telephone.page.scss'],
})
export class ModifierTelephonePage implements OnInit {

    constructor(
        private navController: NavController,
        private popoverController: PopoverController,
        public countriesService: CountriesService,
        public router: Router,
        private userService: UserService,
        public angularFireAuth: AngularFireAuth,
        private platform: Platform
    ) {
    }

    phone: string = '';
    errorPhone = '';

    isIos(){
        return this.platform.is('ios');
    }

    ngOnInit() {
    }

    async ionViewDidEnter() {
        if(!this.isIos()){
            this.userService.recaptchaVerifier = new firebase.auth.RecaptchaVerifier('sign-in-button', {
                size: 'invisible',
                callback: (response) => {

                },
                'expired-callback': () => {
                }
            });
        }
    }

    ionViewDidLoad() {
        if(!this.isIos()){
            this.userService.recaptchaVerifier = new firebase.auth.RecaptchaVerifier('sign-in-button', {
                size: 'invisible',
                callback: (response) => {

                },
                'expired-callback': () => {
                }
            });
        }
    }

    async back() {
        return this.navController.pop();
    }

    async openSelectCountry(ev: any) {
        const popover = await this.popoverController.create({
            component: SelectCountryComponent,
            event: ev,
            translucent: true,
            cssClass: 'popover_class'
        });
        return await popover.present();
    }

    validForm() {
        return this.phone.length < 10;
    }

    valider() {
        if(this.isIos()){
            this.userService.user.phone.callingCode = this.countriesService.selectedCountry.dial_code;
            this.userService.user.phone.phone = this.phone;
            this.userService.save()
                .then(() => this.router.navigate(['principale']))
                .then(() => this.userService.loadingController.dismiss())
                .then(() => this.userService.toast("Votre numéro de téléphone a bien été modifié.", 2000));
        }
        else{
            this.userService.presentLoading("Vous allez recevoir un petit message de notre part");
            this.signInWithPhoneNumber(this.userService.recaptchaVerifier, this.countriesService.selectedCountry.dial_code + this.phone)
                .then(success => {
                    let navigationExtras: NavigationExtras = {
                        queryParams: {
                            dial_code: JSON.stringify(this.countriesService.selectedCountry.dial_code),
                            phone: JSON.stringify(this.phone)
                        }
                    };
                    this.router.navigate(['confirm-phone'], navigationExtras).then(() => {
                        this.userService.loadingController.dismiss();
                    });
                })
                .catch(error => {
                    console.log(error);
                    this.userService.loadingController.dismiss();
                    this.errorPhone = "Numéro de téléphone invalide"
                });
        }
    }

    public signInWithPhoneNumber(recaptchaVerifier, phoneNumber) {
        return new Promise<any>((resolve, reject) => {

            this.angularFireAuth.signInWithPhoneNumber(phoneNumber, recaptchaVerifier)
                .then((confirmationResult) => {
                    this.userService.confirmationResult = confirmationResult;
                    resolve(confirmationResult);
                }).catch((error) => {
                console.log(error);
                reject('SMS not sent');
            });
        });
    }

}
