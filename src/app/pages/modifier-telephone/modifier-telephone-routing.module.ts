import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ModifierTelephonePage } from './modifier-telephone.page';

const routes: Routes = [
  {
    path: '',
    component: ModifierTelephonePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ModifierTelephonePageRoutingModule {}
