import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { StripeNopePage } from './stripe-nope.page';

const routes: Routes = [
  {
    path: '',
    component: StripeNopePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class StripeNopePageRoutingModule {}
