import {Component, OnInit} from '@angular/core';
import {NavigationExtras, Router} from "@angular/router";
import {UserService} from "../../services/user.service";

@Component({
    selector: 'app-stripe-nope',
    templateUrl: './stripe-nope.page.html',
    styleUrls: ['./stripe-nope.page.scss'],
})
export class StripeNopePage implements OnInit {

    constructor(
        public userService: UserService,
        private router: Router,
    ) {
    }

    ngOnInit() {
    }

    goStripe() {
        let navigationExtras: NavigationExtras = {
            replaceUrl: true,
            queryParams: {
                id: this.userService.userInfo.uid
            }
        };
        this.router.navigate(['stripe-account'], navigationExtras);
    }

}
