import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { StripeNopePageRoutingModule } from './stripe-nope-routing.module';

import { StripeNopePage } from './stripe-nope.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    StripeNopePageRoutingModule
  ],
  declarations: [StripeNopePage]
})
export class StripeNopePageModule {}
