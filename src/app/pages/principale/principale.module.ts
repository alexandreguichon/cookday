import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PrincipalePageRoutingModule } from './principale-routing.module';

import { PrincipalePage } from './principale.page';
import {VenteItemComponent} from "../../components/vente-item/vente-item.component";
import {NotationComponent} from "../../components/notation/notation.component";
import {MapComponent} from "../../components/map/map.component";

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        PrincipalePageRoutingModule,
    ],
    exports: [
        VenteItemComponent,
        NotationComponent
    ],
    declarations: [PrincipalePage, VenteItemComponent, NotationComponent, MapComponent]
})
export class PrincipalePageModule {}
