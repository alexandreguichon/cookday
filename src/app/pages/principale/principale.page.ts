import {ChangeDetectorRef, Component, OnInit, ViewChild, ViewRef} from '@angular/core';
import {VentesService} from "../../services/ventes.service";
import {ActivatedRoute, NavigationExtras, Router} from "@angular/router";
import {
    ActionSheetController,
    AlertController,
    IonSlides,
    Platform,
    PopoverController,
    ToastController
} from "@ionic/angular";
import {NotifCenterComponent} from "../../components/notif-center/notif-center.component";
import {UserService} from "../../services/user.service";
import firebase from 'firebase/app';
import {DetailsVentesComponent} from "../../components/details-ventes/details-ventes.component";
import {Camera as CameraCordova, CameraOptions} from "@ionic-native/camera/ngx";
import {Camera, CameraResultType, CameraSource} from "@capacitor/camera";
import {AngularFireStorage} from "@angular/fire/storage";
import {AngularFirestore} from "@angular/fire/firestore";
import {StripeHttpService} from "../../services/stripe-http.service";
import {share, switchMap, take} from "rxjs/operators";
import {AngularFireAuth} from "@angular/fire/auth";
import {FcmService} from "../../services/fcm.service";
import {from} from "rxjs";
import {PushNotifications} from "@capacitor/push-notifications";

@Component({
    selector: 'app-principale',
    templateUrl: './principale.page.html',
    styleUrls: ['./principale.page.scss'],
})
export class PrincipalePage implements OnInit {

    db = firebase.firestore();

    segment: string = "0";
    segmentCuisine: string = "0";
    chosenDate: any = new Date();
    todayDate = new Date();

    recherche: string = "";

    paiementsShownDetailsId: string = "";

    loadingProfilPhoto: boolean = false;

    @ViewChild('slides', {static: true}) slides: IonSlides;
    @ViewChild('slidesCuisine', {static: false}) slidesCuisine: IonSlides;

    slideOpts = {
        effect: 'cube',
    };

    segmentsCuisine = ["Aujourd'hui", "Planning", "Cuisine", "Paiements"];

    boutonsAjouter = {
        "entrees": "une entrée",
        "plats": "un plat",
        "desserts": "un dessert",
        "boissons": "une boisson",
    }

    constructor(
        private cd: ChangeDetectorRef,
        public ventesService: VentesService,
        public router: Router,
        public popoverController: PopoverController,
        public userService: UserService,
        private alertController: AlertController,
        public actionsheetCtrl: ActionSheetController,
        private cameraCordova: CameraCordova,
        private readonly angularFireStorage: AngularFireStorage,
        private readonly angularFirestore: AngularFirestore,
        public stripeHttpService: StripeHttpService,
        public angularFireAuth: AngularFireAuth,
        private fcmService: FcmService,
        private route: ActivatedRoute,
        public readonly platform: Platform,
        private readonly actionSheetController: ActionSheetController,
        private readonly toastController: ToastController
    ) {
    }

    ngOnInit() {
        PushNotifications.checkPermissions().then((res) => console.log(JSON.stringify(res)));
        if (this.getCommandes('à récupérer').length > 0) {
            this.slides.slideTo(1).then(() => {
                this.ventesService.open('commande', this.getCommandes('à récupérer')[0].id)
            });
        }
        this.route.queryParams.subscribe(params => {
            if (params && params.slideTo) {
                this.slides.slideTo(this.maCuisine() ? 3 : 2);
            }
        });
        /*const db = firebase.firestore();
        let self = this;
        db.collection('ventes')
            .get().then(function (querySnapshot) {
            querySnapshot.forEach(vente => {
                let newDoc = vente.data();
                self.ventesService.order.forEach(cat => {
                    newDoc.menu[self.ventesService.retirerAccents(cat)].forEach(plat => {
                        plat.qteAchetee = 0;
                        plat.quantite = 25;
                        plat.quantiteAAjouter = 0
                    })
                })
                db.collection('ventes').doc(vente.id).update(newDoc);
            });
            return true;
        })*/

    }

    ionViewDidEnter() {
        this.verifyStripe();
    }

    verifyStripe() {
        if (this.ventesService.maCuisine() && !this.ventesService.maCuisine().stripeValidated) {
            this.userService.presentLoading("Vérification du compte Stripe en cours...").then(() => {
                this.stripeHttpService.isActiveAccount(this.ventesService.maCuisine().stripeAccountId).pipe(take(1)).toPromise()
                    .then(res => {
                        this.stripeHttpService.accountActivated = res;
                        if (!res) {
                            this.router.navigate(['stripe-nope']).then(() => {
                                this.userService.loadingController.dismiss();
                            });
                        } else {
                            this.ventesService.maCuisine()['stripeValidated'] = true;
                            this.db.collection('cuisines').doc(this.userService.user.id).update({
                                stripeValidated: true
                            })
                            this.userService.loadingController.dismiss().then(() => {
                                this.detectChanges();
                            });
                        }
                    });
            })
        } else if (this.ventesService.maCuisine() && this.ventesService.maCuisine().stripeValidated) {
            this.stripeHttpService.accountActivated = true;
        }
    }

    onSlideChange(event: any) {
        let self = this;
        event.then(function (result) {
            self.segment = result.toString();
            self.detectChanges();
        });
    }

    onSlideCuisineChange(event: any) {
        let self = this;
        event.then(function (result) {
            self.segmentCuisine = result.toString();
            self.detectChanges();
        });
    }

    detectChanges() {
        setTimeout(() => {
            if (this.cd && !(this.cd as ViewRef).destroyed) {
                this.cd.detectChanges();
            }
        });
    }

    Number(segment: string) {
        return +segment;
    }

    isActive(index) {
        return index == this.Number(this.segment) ? 1 : 0;
    }

    isToday(date) {
        var todayDate = new Date();
        return date.getFullYear() == todayDate.getFullYear()
            && date.getMonth() == todayDate.getMonth()
            && date.getDate() == todayDate.getDate();
    }

    isTomorrow(date) {
        var todayDate = new Date();
        return date.getFullYear() == todayDate.getFullYear()
            && date.getMonth() == todayDate.getMonth()
            && date.getDate() == todayDate.getDate() + 1;
    }

    decrementDate() {
        this.chosenDate = new Date(this.chosenDate.getTime() - 86400000);
        this.detectChanges();
    }

    incrementDate() {
        this.chosenDate = new Date(this.chosenDate.getTime() + 86400000);
        this.detectChanges();
    }

    goRecherche() {
        this.router.navigate(['tabs']);
    }

    async presentNotifyPopover(ev: any) {
        const popover = await this.popoverController.create({
            component: NotifCenterComponent,
            event: ev,
            translucent: true
        });
        return await popover.present();
    }

    getCommandes(statut) {
        let res = Object.values(this.userService.commandesData as any[]);
        res = res.filter(c => c.statut == statut).sort((a: any, b: any) => a.date - b.date);
        return res;
    }

    getAllCommandes() {
        let res = Object.values(this.userService.commandesData as any[]);
        return res.sort((a: any, b: any) => b.dateRes - a.dateRes);
    }

    getCommandeCuisine(commande) {
        return this.ventesService.cuisines[commande.cuisine];
    }

    firstUp(str) {
        return str.charAt(0).toUpperCase() + str.slice(1).toLowerCase();
    }

    async openSupport() {
        let to = "cookday.support@gmail.com";
        let subject = 'Support';
        let body = 'id : ' + this.userService.userInfo.uid + '%0D%0A %0D%0A';
        body += 'Description de votre problème : %0D%0A %0D%0A';
        window.location.href = 'mailto:' + to + '?subject=' + subject + '&body=' + body;
    }

    openAPropos(){
        this.router.navigate(['a-propos']);
    }

    openDevenirCuisine() {
        this.router.navigate(['devenir-cuisine']);
    }

    openModifierTelephone() {
        this.router.navigate(['modifier-telephone']);
    }

    goCuisine() {
        this.slides.slideTo(0);
//         this.openDevenirCuisine();
    }

    numSegment(val) {
        return (this.userService.userInfo?.uid && this.ventesService.maCuisine() && this.stripeHttpService.accountActivated) ? val : (val - 1);
    }

    maCuisine() {
        return this.ventesService.maCuisine();
    }

    articleItemBorder(cat, i) {
        return i >= this.maCuisine().menu[this.ventesService.retirerAccents(cat)].length - 1 ? 'unset' : '1px solid var(--ion-color-medium)';
    }

    openAjouterPlat(cat: string) {
        let navigationExtras: NavigationExtras = {
            queryParams: {
                cat: JSON.stringify(cat),
            }
        };
        this.router.navigate(['ajouter-plat-cuisine'], navigationExtras);
    }

    openEditCuisine() {
        this.router.navigate(['modifier-cuisine']);
    }

    goLogin() {
        this.router.navigate(['login']);
    }

    editPlat(cat, plat) {
        let navigationExtras: NavigationExtras = {
            queryParams: {
                cat: JSON.stringify(cat),
                plat: JSON.stringify(plat)
            }
        };
        this.router.navigate(['ajouter-plat-cuisine'], navigationExtras);
    }

    nouvelleVente() {
        // this.fcmService.sendChatNotify("Test notif", "", this.userService.userInfo.uid, this.userService.userInfo.uid);
        if (this.ventesService.getPlats(this.ventesService.maCuisine()).length == 0) {
            this.alertPasDePlats();
        } else {
            this.router.navigate(['nouvelle-vente']);
        }
    }

    async alertPasDePlats() {
        const alert = await this.alertController.create({
            header: "Pas de plats au menu",
            message: "Aucun plat n'a été ajouté dans la cuisine. Complétez votre menu avant de proposer des ventes.",
            buttons: [
                {
                    text: 'Annuler',
                    role: 'cancel',
                    cssClass: 'color-medium',
                    handler: (blah) => {
                    }
                },
                {
                    text: 'Ajouter des plats',
                    cssClass: 'color-first',
                    handler: () => {
                        this.segmentCuisine = "2";
                    }
                }
            ]
        });

        await alert.present();
    }

    editVente(vente) {
        let navigationExtras: NavigationExtras = {
            queryParams: {
                vente: JSON.stringify(vente)
            }
        };
        this.router.navigate(['nouvelle-vente'], navigationExtras);
    }

    async presentDetailsVente(vente, ev) {
        const popover = await this.popoverController.create({
            component: DetailsVentesComponent,
            translucent: true,
            componentProps: {
                vente: vente
            },
            event: ev
        });
        return await popover.present();
    }

    goPaiements() {
        this.slidesCuisine.slideTo(3);
    }

    prochaineVenteAujourdhui() {
        return this.ventesAujourdhui().find(v => v.horaires.fin > new Date());
    }

    ventesAujourdhui() {
        let res = Object.values(this.ventesService.ventes) as any[];
        return res.filter(vente => vente.cuisine == this.userService.userInfo.uid && this.isToday(vente.date)).sort((a: any, b: any) => a.horaires.debut - b.horaires.debut);
    }

    commandesParStatut(vente: string, statut: string) {
        let res = Object.values(this.userService.commandesRecues) as any[];
        return res.filter(commande => commande.vente == vente && commande.statut == statut).sort((a: any, b: any) => a.dateRecup - b.dateRecup);
    }

    async changerStatutCommande(commande: any, statut: string) {
        const alert = await this.alertController.create({
            header: "Demande de confirmation",
            message: "Confirmez vous que la commande est " + statut + "?",
            buttons: [
                {
                    text: 'Annuler',
                    role: 'cancel',
                    cssClass: 'secondary',
                    handler: (blah) => {
                    }
                },
                {
                    text: 'Confirmer',
                    handler: () => {
                        this.userService.setCommandeStatut(commande, statut).then(() => {
                            this.fcmService.sendCommandeNotify(commande, statut)
                        });
                    }
                }
            ]
        });

        await alert.present();
    }

    getProfileImg() {
        if (this.loadingProfilPhoto) {
            return '';
        } else {
            return this.userService.user.photo ? this.userService.user.photo : "assets/addPhoto.jpg";
        }
    }

    async openPhotoChoice() {
        if (this.platform.is('capacitor')) {
            const actionSheet = await this.actionSheetController.create({
                cssClass: 'action-sheets-basic-page',
                buttons: [
                    {
                        text: 'Prendre une photo',
                        role: 'destructive',
                        icon: 'camera-outline',
                        handler: () => this.getImageFromSource(CameraSource.Camera)
                    },
                    {
                        text: 'Choisir dans la galerie',
                        icon: 'images-outline',
                        handler: () => this.getImageFromSource(CameraSource.Photos)
                    },
                ]
            });
            await actionSheet.present();
        } else {
            this.getImageFromSource(CameraSource.Photos);
        }
    }

    savePhoto(path: string) {
        this.loadingProfilPhoto = true;
        return new Promise<any>((resolve, reject) => {
            let imageRef = this.angularFireStorage.ref(this.userService.userInfo.uid + '/photoProfil.jpg');
            let self = this;
            this.encodeImageUri(path, function (image64) {
                imageRef.putString(image64, 'data_url')
                    .then(snapshot => {
                        imageRef.getDownloadURL().toPromise().then((lienImg) => {
                            self.userService.user.photo = lienImg;
                            self.angularFirestore.doc("utilisateurs/" + self.userService.userInfo.uid).update({
                                photo: lienImg
                            }).then(function () {
                                self.loadingProfilPhoto = false;
                            });
                        });
                    }, err => {
                        reject(err);
                    });
            });
        });
    }

    encodeImageUri(imageUri, callback) {
        var c = document.createElement('canvas');
        var ctx = c.getContext('2d');
        var img = new Image();
        img.onload = function () {
            var aux: any = this;
            c.width = aux.width;
            c.height = aux.height;
            ctx.drawImage(img, 0, 0);
            var dataURL = c.toDataURL('image/jpeg');
            callback(dataURL);
        };
        img.src = imageUri;
    };

    getUserImg(commande: any) {
        return commande.userData.photo ? commande.userData.photo : "assets/unknown.jpg";
    }

    test() {
        this.ventesService.open('noter-commande', 'AiXkGdBIL5rDZt7WJfSL');
    }

    addPhoto() {
        this.getImageFromSource(CameraSource.Camera);
    }

    private getImageFromSource(source: CameraSource): void {
        Camera.getPhoto({
            resultType: CameraResultType.Uri,
            source,
        }).then((pic) => {
            this.savePhoto(pic.webPath);
        });
    }

    changeRecherche(ev: any) {
        this.recherche = ev.detail.value;
        this.detectChanges();
    }

    textRes() {
        let res = '"' + this.recherche + '" ' + this.ventesService.searchVentes(this.recherche).length + ' résultat';
        if (this.ventesService.searchVentes(this.recherche).length > 1) {
            res = res + 's'
        }
        return res;
    }

    showDate(vente, index) {
        return index == 0 || !this.sameDate(this.ventesService.searchVentes(this.recherche)[index - 1].date, vente.date);
    }

    sameDate(date1, date2) {
        return date1.getFullYear() == date2.getFullYear()
            && date1.getMonth() == date2.getMonth()
            && date1.getDate() == date2.getDate();
    }

    openMap() {
        this.router.navigate(['map']);
    }

    openChangeMdp(){
        this.router.navigate(['change-mdp']);
    }

    testNotify(){
        this.fcmService.sendPostRequest(
            'Test notif',
            "Votre commande est passé au statut",
            "https://firebasestorage.googleapis.com/v0/b/cv-generator-5bfe9.appspot.com/o/entreprise.png?alt=media&token=a05c7135-0dfb-43a4-a713-f7db130af814",
            {},
            "",
            this.userService.userInfo.uid,
            "75EF66F6B0EBEEDD28A0B6D1AC4AF9A31A0852C82BC2A6D87C596B2E7E0A6F46",
            "",
            "commande",
            this.userService.userInfo.uid)
    }
}
