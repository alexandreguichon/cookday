import {ChangeDetectorRef, Component, NgZone, OnInit, ViewChild, ViewRef} from '@angular/core';
import {
    ActionSheetController,
    AlertController,
    IonInput,
    IonTextarea,
    LoadingController,
    NavController, Platform
} from "@ionic/angular";
import {Camera as CameraCordova, CameraOptions} from "@ionic-native/camera/ngx";
import {Camera, CameraResultType, CameraSource} from "@capacitor/camera";
import {VentesService} from "../../services/ventes.service";
import firebase from 'firebase/app';
import {UserService} from "../../services/user.service";

@Component({
    selector: 'app-modifier-cuisine',
    templateUrl: './modifier-cuisine.page.html',
    styleUrls: ['./modifier-cuisine.page.scss'],
})
export class ModifierCuisinePage implements OnInit {

    @ViewChild('input') myInput: IonInput;
    @ViewChild('textarea') myDescription: IonTextarea;

    GooglePlaces: google.maps.places.PlacesService;
    db = firebase.firestore();

    constructor(
        private navController: NavController,
        private alertController: AlertController,
        private cd: ChangeDetectorRef,
        public actionsheetCtrl: ActionSheetController,
        private cameraCordova: CameraCordova,
        public ventesService: VentesService,
        private loadingController: LoadingController,
        public zone: NgZone,
        public userService: UserService,
        private readonly actionSheetController: ActionSheetController,
        public readonly platform: Platform,
    ) {
    }

    GoogleAutocomplete: google.maps.places.AutocompleteService;
    autocomplete: { input: string; };
    autocompleteItems: any[];
    inputFocused: boolean = false;
    selectedPlace: boolean =false;

    myPhotos = [];

    ngOnInit() {
        this.GoogleAutocomplete = new google.maps.places.AutocompleteService();
        this.autocompleteItems = [];
        if (this.maCuisine().adresse && this.maCuisine().adresse != '') {
            this.autocomplete = {input: this.maCuisine().adresse};
            this.updateSearchResults();
        } else {
            this.autocomplete = {input: ''};
        }
        this.maCuisine().photos.forEach(photo => {
            this.myPhotos.push({
                img: photo,
                clicked: false
            })
        });
    }

    back() {
        this.navController.pop();
    }

    async showDeletePhotoAlert(photoIndex: number) {
        const alert = await this.alertController.create({
            header: "Supprimer la photo",
            message: "Es-tu sûr de vouloir supprimer la photo?",
            buttons: [
                {
                    text: "Annuler",
                    role: 'cancel',
                    handler: (blah) => {
                        this.myPhotos[photoIndex].clicked = false;
                    }
                },
                {
                    text: "Confirmer",
                    handler: () => {
                        this.deletePhoto(photoIndex);
                    }
                }
            ]
        });

        await alert.present();
    }

    deletePhoto(photoIndex){
        this.myPhotos.splice(photoIndex, 1);
        let storageRef = firebase.storage();
        storageRef.refFromURL(this.maCuisine().photos[photoIndex]).delete().then(()=>{
            this.maCuisine().photos.splice(photoIndex, 1);
            this.db.collection('cuisines').doc(this.maCuisine().id).update({
                photos: this.maCuisine().photos
            });
        });
    }

    detectChanges() {
        if (this.cd && !(this.cd as ViewRef).destroyed) {
            this.cd.detectChanges();
        }
    }

    async openPhotoChoice() {
        if (this.platform.is('capacitor')) {
            const actionSheet = await this.actionSheetController.create({
                cssClass: 'action-sheets-basic-page',
                buttons: [
                    {
                        text: 'Prendre une photo',
                        role: 'destructive',
                        icon: 'camera-outline',
                        handler: () => this.getImageFromSource(CameraSource.Camera)
                    },
                    {
                        text: 'Choisir dans la galerie',
                        icon: 'images-outline',
                        handler: () => this.getImageFromSource(CameraSource.Photos)
                    },
                ]
            });
            await actionSheet.present();
        } else {
            this.getImageFromSource(CameraSource.Photos);
        }
    }

    private getImageFromSource(source: CameraSource): void {
        Camera.getPhoto({
            resultType: CameraResultType.Uri,
            source,
        }).then((pic) => {
            this.addPhoto(pic.webPath);
        });
    }

    addPhoto(path){
        this.presentLoading("Enregistrement de la photo").then(()=>{
            return new Promise<any>((resolve, reject) => {
                let storageRef = firebase.storage().ref();
                let imageRef = storageRef.child('Cuisines').child(this.makeid(30)+'.jpg');
                let self = this;
                this.encodeImageUri(path, function (image64) {
                    imageRef.putString(image64, 'data_url')
                        .then(snapshot => {
                            imageRef.getDownloadURL().then((lienImg) => {
                                self.myPhotos.push({
                                    img: lienImg,
                                    clicked: false
                                });
                                self.maCuisine().photos.push(lienImg);
                                self.db.collection('cuisines').doc(self.maCuisine().id).update({
                                    photos: self.maCuisine().photos
                                }).then(function () {
                                    self.loadingController.dismiss();
                                });
                            });
                        }, err => {
                            reject(err);
                        });
                });
            });
        })
    }

    makeid(length) {
        var result           = '';
        var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        var charactersLength = characters.length;
        for ( var i = 0; i < length; i++ ) {
            result += characters.charAt(Math.floor(Math.random() *
                charactersLength));
        }
        return result;
    }

    encodeImageUri(imageUri, callback) {
        var c = document.createElement('canvas');
        var ctx = c.getContext('2d');
        var img = new Image();
        img.onload = function () {
            var aux: any = this;
            c.width = aux.width;
            c.height = aux.height;
            ctx.drawImage(img, 0, 0);
            var dataURL = c.toDataURL('image/jpeg');
            callback(dataURL);
        };
        img.src = imageUri;
    };

    buttonDisabled() {
        return false;
        //return this.nom == '' || this.adresse == '' || this.myPhotos.length==0;
    }

    valider() {
        /*this.userService.presentLoading("Recherche de l'adresse");
        let options: NativeGeocoderOptions = {
            useLocale: true,
            maxResults: 1
        };
        let locationData: any;
        let lienMaps = "https://www.google.com/maps/search/".concat(this.adresse).split(' ').join('+');
        if(this.platform.is('capacitor')){
            this.nativeGeocoder.forwardGeocode(lienMaps, options)
                .then((result: NativeGeocoderResult[]) => {
                    locationData = new firebase.firestore.GeoPoint(Number(result[0].latitude), Number(result[0].longitude));
                    let navigationExtras: NavigationExtras = {
                        queryParams: {
                            nom: JSON.stringify(this.nom),
                            adresse: JSON.stringify(this.adresse),
                            description: JSON.stringify(this.description),
                            geoloc: JSON.stringify(locationData),
                            photos: JSON.stringify((this.myPhotos))
                        }
                    };
                    this.router.navigate(['entrer-infos-bancaires'], navigationExtras).then(()=>{
                        this.userService.loadingController.dismiss();
                    });
                })
                .catch((error: any) => {
                    this.userService.loadingController.dismiss();
                    alert("Aucun lieu correspondant à cette adresse n'a été trouvé.");
                });
        }*/
    }

    maCuisine() {
        return this.ventesService.maCuisine();
    }

    setPhotoClicked(photo){
        if(this.myPhotos.length>1){
            photo.clicked = true;
        }
    }

    onNameChange(){
        this.db.collection('cuisines').doc(this.maCuisine().id).update({
            nom: this.maCuisine().nom
        })
    }

    onDescriptionChange(){
        this.db.collection('cuisines').doc(this.maCuisine().id).update({
            description: this.maCuisine().description
        })
    }

    async presentLoading(message) {
        const loading = await this.loadingController.create({
            message: message
        });
        await loading.present();
    }

    updateSearchResults() {
        if (this.autocomplete.input == '') {
            this.autocompleteItems = [];
            return;
        }
        this.GoogleAutocomplete.getPlacePredictions({input: this.autocomplete.input},
            (predictions, status) => {
                this.autocompleteItems = [];
                this.zone.run(() => {
                    predictions.forEach((prediction) => {
                        this.autocompleteItems.push(prediction);
                    });
                    this.cd.detectChanges();
                });
            });
    }

    focusOut(){
        this.inputFocused = false;
        this.myDescription.setFocus().then(r => {
        });
        if(!this.selectedPlace){
            this.autocomplete.input = this.maCuisine().adresse;
        }
        this.selectedPlace = false;
    }

    selectSearchResult(item) {
        const browserMap = new google.maps.Map(document.createElement('map_canvas'), null);
        this.GooglePlaces = new google.maps.places.PlacesService(browserMap);
        const request = {
            placeId: item.place_id,
            fields: ['geometry']
        };
        this.GooglePlaces.getDetails(request, (results, status) => {
            this.selectedPlace = true;
            this.autocomplete.input = item.description;
            this.focusOut();
            this.maCuisine().adresse = item.description;
            this.maCuisine().geoloc = {
                latitude: results.geometry.location.lat(),
                longitude: results.geometry.location.lng()
            }
            this.db.collection('cuisines').doc(this.maCuisine().id).update({
                adresse: this.maCuisine().adresse,
                geoloc: this.maCuisine().geoloc
            })
        });
    }

    onAutoFormatChanged() {
        this.maCuisine().nom = this.setFirstLetterToUppercase(this.maCuisine().nom);
        this.cd.detectChanges();
    }

    onAutoFormatChangedDescription() {
        this.maCuisine().description = this.setFirstLetterToUppercase(this.maCuisine().description);
        this.cd.detectChanges();
    }

    setFirstLetterToUppercase(string:string):string {
        return string.charAt(0).toUpperCase() + string.slice(1);
    }

}
