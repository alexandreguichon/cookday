import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ModifierCuisinePage } from './modifier-cuisine.page';

const routes: Routes = [
  {
    path: '',
    component: ModifierCuisinePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ModifierCuisinePageRoutingModule {}
