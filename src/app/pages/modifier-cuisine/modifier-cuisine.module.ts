import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ModifierCuisinePageRoutingModule } from './modifier-cuisine-routing.module';

import { ModifierCuisinePage } from './modifier-cuisine.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ModifierCuisinePageRoutingModule
  ],
  declarations: [ModifierCuisinePage]
})
export class ModifierCuisinePageModule {}
