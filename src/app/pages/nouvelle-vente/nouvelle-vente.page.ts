import {Component, OnInit} from '@angular/core';
import {NavController, PopoverController} from "@ionic/angular";
import {VentesService} from "../../services/ventes.service";
import {AddElementVenteComponent} from "../../components/add-element-vente/add-element-vente.component";
import {UserService} from "../../services/user.service";
import firebase from 'firebase/app';
import {ActivatedRoute, Router} from "@angular/router";

@Component({
    selector: 'app-nouvelle-vente',
    templateUrl: './nouvelle-vente.page.html',
    styleUrls: ['./nouvelle-vente.page.scss'],
})
export class NouvelleVentePage implements OnInit {

    db = firebase.firestore();

    editing: boolean = false;
    venteData: any;

    nowDate = new Date();

    currentDate: any;
    currentHourDebut: any;
    currentHourFin: any;

    constructor(
        private navController: NavController,
        public ventesService: VentesService,
        private popoverController: PopoverController,
        public userService: UserService,
        private router: Router,
        private route: ActivatedRoute
    ) {
        this.currentDate = this.nowDate.toISOString();
        this.currentHourDebut = this.nowDate.toISOString();
        this.currentHourFin = this.nowDate.toISOString();
        this.route.queryParams.subscribe(params => {
            if (params && params.vente) {
                this.editing = true;
                this.venteData = JSON.parse(params.vente);
                this.ventesService.newVente.date = new Date(this.venteData.date);
                this.currentDate = this.ventesService.newVente.date.toISOString();
                this.ventesService.newVente.heureDebut = this.venteData.horaires.debut;
                this.currentHourDebut = new Date(this.ventesService.newVente.heureDebut).toISOString();
                this.ventesService.newVente.heureFin = this.venteData.horaires.fin;
                this.currentHourFin = new Date(this.ventesService.newVente.heureFin).toISOString();
                this.ventesService.newVente.menu = this.venteData.menu;

            }
        });
    }

    ngOnInit() {
    }

    ngOnDestroy(){
        this.editing = false;
        this.venteData = null;
        this.ventesService.newVente = {
            date: new Date(),
            heureDebut: new Date(),
            heureFin: new Date(),
            menu: {
                entrees : [],
                plats: [],
                desserts: [],
                boissons: []
            }
        }
    }

    back() {
        this.navController.pop();
    }

    changeDate($event) {
        this.ventesService.newVente.date = $event.detail.value;
    }

    changeHeureDebut($event) {
        this.ventesService.newVente.heureDebut = $event.detail.value;
    }

    changeHeureFin($event) {
        this.ventesService.newVente.heureFin = $event.detail.value;
    }


    toISOString(date: any) {
        let newDate = new Date(date);
        newDate.setHours(newDate.getHours() + 1);
        return newDate.toISOString();
    }

    isEmpty() {
        let res = 0;
        Object.values(this.ventesService.newVente.menu).forEach(cat => {
            res = res + cat.length;
        })
        return res == 0;
    }

    async presentAddElement() {
        const popover = await this.popoverController.create({
            component: AddElementVenteComponent,
            translucent: true,
        });
        return await popover.present();
    }

    tousLesElementsDansVente() {
        let tailleVente = 0;
        Object.values(this.ventesService.newVente.menu).forEach(cat => {
            tailleVente = tailleVente + cat.length;
        })
        let tailleMenu = 0;
        let toArrayMenu: any[] = Object.values(this.ventesService.maCuisine().menu);
        Object.values(toArrayMenu).forEach(cat => {
            tailleMenu = tailleMenu + cat.length;
        });
        return tailleVente >= tailleMenu;
    }

    disabledBtn() {
        let tailleVente = 0;
        Object.values(this.ventesService.newVente.menu).forEach(cat => {
            tailleVente = tailleVente + cat.length;
        })
        if (tailleVente == 0) {
            return true;
        } else {
            let toArrayMenu: any[] = Object.values(this.ventesService.newVente.menu);
            let res: boolean = false;
            Object.values(toArrayMenu).forEach(cat => {
                if (cat.some(plat => !plat.quantite || plat.quantite == 0)) {
                    res = true;
                }
            });
            return res;
        }
    }

    ajouterVente() {
        this.ventesService.presentLoading("On met les petits plats dans les grands...").then(() => {
            let newVente = {commandes: []};
            newVente["cuisine"] = this.userService.userInfo.uid;
            newVente["date"] = firebase.firestore.Timestamp.fromDate(new Date(this.ventesService.newVente.date));
            newVente["horaires"] = {
                debut: firebase.firestore.Timestamp.fromDate(new Date(this.ventesService.newVente.heureDebut)),
                fin: firebase.firestore.Timestamp.fromDate(new Date(this.ventesService.newVente.heureFin)),
            };
            newVente["menu"] = this.ventesService.newVente.menu;
            if (!this.editing) {
                this.db.collection("ventes").add(newVente).then(res => {
                    if (this.ventesService.maCuisine().ventesProposees) {
                        this.ventesService.maCuisine().ventesProposees.push(res.id);
                    } else {
                        this.ventesService.maCuisine().ventesProposees = [res.id];
                    }
                    Promise.all([
                        this.db.collection("cuisines").doc(this.userService.userInfo.uid).update({
                            ventesProposees: this.ventesService.maCuisine().ventesProposees
                        }),
                        this.db.collection("ventes").doc(res.id).update({
                            id: res.id
                        })
                    ]).then(() => {
                        this.navController.pop().then(() => {
                            this.ventesService.loadingController.dismiss();
                            this.ventesService.toast("La vente a été créée.", 2000);
                        });
                    })
                })
            } else {
                this.db.collection("ventes").doc(this.venteData.id).update(newVente).then(()=>{
                    this.back();
                    this.ventesService.loadingController.dismiss();
                    this.ventesService.toast("La vente a été modifiée.", 2000);
                })
            }
        });
    }
}
