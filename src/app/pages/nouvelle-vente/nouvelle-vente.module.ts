import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { NouvelleVentePageRoutingModule } from './nouvelle-vente-routing.module';

import { NouvelleVentePage } from './nouvelle-vente.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    NouvelleVentePageRoutingModule
  ],
  declarations: [NouvelleVentePage]
})
export class NouvelleVentePageModule {}
