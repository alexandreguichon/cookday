import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { NouvelleVentePage } from './nouvelle-vente.page';

const routes: Routes = [
  {
    path: '',
    component: NouvelleVentePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class NouvelleVentePageRoutingModule {}
