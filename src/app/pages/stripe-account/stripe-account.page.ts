import {Component, NgZone, OnInit} from '@angular/core';
import firebase from 'firebase/app';
import {StripeHttpService} from "../../services/stripe-http.service";
import {UserService} from "../../services/user.service";
import {ActivatedRoute, Router} from "@angular/router";
import {fromPromise} from "rxjs/internal-compatibility";
import {map, switchMap, tap} from "rxjs/operators";
import {iif, Observable, of} from "rxjs";

@Component({
    selector: 'app-stripe-account',
    templateUrl: './stripe-account.page.html',
    styleUrls: ['./stripe-account.page.scss'],
})
export class StripeAccountPage implements OnInit {

    id = "";

    showButton: boolean = false;

    db = firebase.firestore()

    constructor(private readonly stripeHttpService: StripeHttpService,
                private readonly userService: UserService,
                private route: ActivatedRoute,
                private router: Router,
                public ngZone: NgZone
    ) {
        this.route.queryParams.subscribe(params => {
            if (params && params.id) {
                this.id = params.id;
                this.initActions();
            }
        });
        let self = this;
        setTimeout(function () {
            self.showButton = true;
        }, 10000);
    }

    ngOnInit() {

    }

    initActions() {
        this.ngZone.run(() => {
            fromPromise(this.db.collection('cuisines').doc(this.id).get()).pipe(
                map((doc) => doc.data().stripeAccountId),
                switchMap((accountId) => iif(() => !accountId, this.createAccount(), of(accountId))),
                switchMap((accountId) => this.stripeHttpService.createAccountLink(accountId, this.id)),
                tap({error: (err) => console.log(JSON.stringify(err))}),
                tap((accountLink) => window.location.assign(accountLink.url))
            ).subscribe();
        })
    }

    private createAccount(): Observable<string> {
        return this.stripeHttpService.createAccount().pipe(
            map((account) => account.id),
            tap((accountId) => {
                this.db.collection('cuisines').doc(this.userService.userInfo.uid).update({
                    stripeAccountId: accountId
                });
            })
        );
    }

    goPrincipale() {
        this.router.navigate(['principale']);
    }
}
