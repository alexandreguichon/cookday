import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { StripeAccountPage } from './stripe-account.page';

const routes: Routes = [
  {
    path: '',
    component: StripeAccountPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class StripeAccountPageRoutingModule {}
