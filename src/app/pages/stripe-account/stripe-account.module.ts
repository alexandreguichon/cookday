import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { StripeAccountPageRoutingModule } from './stripe-account-routing.module';

import { StripeAccountPage } from './stripe-account.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    StripeAccountPageRoutingModule
  ],
  declarations: [StripeAccountPage]
})
export class StripeAccountPageModule {}
