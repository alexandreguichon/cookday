import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";
import {AngularFireAuth} from "@angular/fire/auth";
import {LoadingController, ToastController} from "@ionic/angular";
import {UserService} from "../../services/user.service";

@Component({
  selector: 'app-change-mdp',
  templateUrl: './change-mdp.page.html',
  styleUrls: ['./change-mdp.page.scss'],
})
export class ChangeMdpPage implements OnInit {

  mdp: string = "";
  mdpError: string = "";
  newMdp: string = "";
  newMdpError: string = "";

  constructor(
      private router: Router,
      public angularFireAuth: AngularFireAuth,
      private userService: UserService,
      public toastCtrl: ToastController,
      private loadingController: LoadingController,
  ) { }

  ngOnInit() {
  }

  back() {
    this.router.navigate(["principale"]);
  }

  async changeMdp() {
    this.mdpError = '';
    this.newMdpError = '';
    this.presentLoading("Vérification du mot de passe...").then(() => this.angularFireAuth.signInWithEmailAndPassword(this.userService.userInfo.email, this.mdp)).then(() => {
      this.loadingController.dismiss()
          .then(() => this.presentLoading("Modification du mot de passe..."))
          .then(() => this.angularFireAuth.currentUser)
          .then(user => user.updatePassword(this.newMdp))
          .then(() => this.back())
          .then(() => this.loadingController.dismiss())
          .then(() => this.toast("Le mot de passe a été modifié.", 2000))
    }).catch((error) => {
      this.mdpError = "Mot de passe incorrect";
      this.loadingController.dismiss();
    });

  }

  async toast(text: string, duration: number) {
    return await this.toastCtrl.create({
      message: text,
      duration: duration,
      position: 'top',
      cssClass: 'dark-trans'
    });
  }

  async presentLoading(message) {
    const loading = await this.loadingController.create({
      message: message
    });
    await loading.present();
  }

}
