import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ChangeMdpPageRoutingModule } from './change-mdp-routing.module';

import { ChangeMdpPage } from './change-mdp.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ChangeMdpPageRoutingModule
  ],
  declarations: [ChangeMdpPage]
})
export class ChangeMdpPageModule {}
