import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ChangeMdpPage } from './change-mdp.page';

const routes: Routes = [
  {
    path: '',
    component: ChangeMdpPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ChangeMdpPageRoutingModule {}
