import {ChangeDetectorRef, Component, ElementRef, OnInit, ViewChild, ViewRef} from '@angular/core';
import {UserService} from "../../services/user.service";
import {VentesService} from "../../services/ventes.service";
import {NavigationExtras, Router} from "@angular/router";
import {CapacitorGoogleMaps} from "@capacitor-community/google-maps";
import {Platform} from "@ionic/angular";
import {strategy} from "@angular-devkit/core/src/experimental/jobs";

@Component({
    selector: 'app-map',
    templateUrl: './map.page.html',
    styleUrls: ['./map.page.scss'],
})
export class MapPage implements OnInit {

    @ViewChild('container') mapView: ElementRef;

    constructor(
        private userService: UserService,
        private ventesService: VentesService,
        private router: Router,
        private cd: ChangeDetectorRef,
        private platform: Platform
    ) {
    }

    ngOnInit() {

    }

    ionViewDidEnter() {
        CapacitorGoogleMaps.initialize({
            key: 'AIzaSyDYUDht_2vJ-1vqBzozSZa6EcbRvWp0NOs',
            devicePixelRatio: window.devicePixelRatio
        }).then(() => {
            this.createMap()
        })
    }

    createMap() {
        // const element = document.getElementById("container");
        // const boundingRect = element.getBoundingClientRect();

        const boundingRect = this.mapView.nativeElement.getBoundingClientRect() as DOMRect;


        CapacitorGoogleMaps.createMap({
            cameraPosition: {
                target: {
                    latitude: this.userService.userlat,
                    longitude: this.userService.userlng,
                },
                zoom: 12
            },
            boundingRect: {
                width: Math.round(boundingRect.width),
                height: Math.round(boundingRect.height),
                x: Math.round(boundingRect.x),
                y: Math.round(boundingRect.y),
            },
            preferences: {
                appearance: {
                    type: 1
                }
            }
        }).then((res) => {
            this.mapView.nativeElement.style.background = "";

            // finally set `data-maps-id` attribute for delegating touch events
            this.mapView.nativeElement.setAttribute("data-maps-id", res.googleMap.mapId);

            this.userService.map = res.googleMap.mapId;

                this.showCurrentPosition()
                    .then(() => {
                        this.showCuisines()
                    });

                CapacitorGoogleMaps.didTapMarker({
                    mapId: this.userService.map
                }, async (ev) => {
                    if (ev.marker) {
                        await this.openCuisine(ev.marker.preferences.metadata.id);
                    }
                });
        });

    }

    async showCurrentPosition() {
        // Create our current location marker
        await CapacitorGoogleMaps.addMarker({
            mapId: this.userService.map,
            position: {
                latitude: this.userService.userlat,
                longitude: this.userService.userlng,
            }
        });
    }

    async showCuisines() {
        let res = Object.values(this.ventesService.cuisines) as any[];
        res.forEach(cuisine => {
            CapacitorGoogleMaps.addMarker({
                mapId: this.userService.map,
                position: {
                    latitude: cuisine.geoloc.latitude,
                    longitude: cuisine.geoloc.longitude
                },
                preferences: {
                    title: cuisine.nom,
                    metadata: {id: cuisine.id},
                    icon: {
                        url: 'https://firebasestorage.googleapis.com/v0/b/cookday-9366d.appspot.com/o/Icones%20marqueurs%2FMarqueur_Cookday_proposition2%2050px.png?alt=media&token=85830fce-6a86-4264-a39f-f9a5a72ceb9e',
                        size: {
                            height: 50,
                            width: 50
                        }
                    }
                }
            })
        });
        return true;
    }


    ionViewWillLeave() {
        CapacitorGoogleMaps.removeMap({
            mapId: this.userService.map
        });
    }

    openCuisine(id) {
        console.log('prout', id)
        this.ventesService.open('cuisine', id);
        let navigationExtras: NavigationExtras = {
            replaceUrl: true,
            queryParams: {
                id: JSON.stringify(id)
            }
        };
        return this.router.navigate(['cuisine'], navigationExtras);
    }

    back() {
        CapacitorGoogleMaps.removeMap({
            mapId: this.userService.map
        });
        this.router.navigate(['principale'])
            .then(() => this.detectChanges());
    }

    detectChanges() {
        setTimeout(() => {
            if (this.cd && !(this.cd as ViewRef).destroyed) {
                this.cd.detectChanges();
            }
        });
    }

}
