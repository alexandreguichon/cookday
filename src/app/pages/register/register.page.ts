import {Component, OnInit} from '@angular/core';
import {NavigationExtras, Router} from "@angular/router";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import firebase from 'firebase/app';
import {AngularFireAuth} from "@angular/fire/auth";
import {AlertController, Platform, PopoverController} from "@ionic/angular";
import {SelectCountryComponent} from "../../components/select-country/select-country.component";
import {UserService} from "../../services/user.service";
import {CountriesService} from "../../services/countries.service";
import {catchError} from "rxjs/operators";

@Component({
    selector: 'app-register',
    templateUrl: './register.page.html',
    styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {

    registerForm: FormGroup;

    formErrors = {
        'prenom': '',
        'nom': '',
        'email': '',
        'password': '',
        'cpassword': '',
    };
    validationMessages = {
        'prenom': {
            'required': 'Un pseudo est requis',
        },
        'nom': {
            'required': 'Un pseudo est requis',
        },
        'phone': {
            'required': "Un numéro de téléphone est requis",
            'email': "Le numéro de téléphone n'est pas valide"
        },
        'email': {
            'required': "Une adresse email est requise",
            'email': "L'adresse email n'est pas valide"
        },
        'password': {
            'required': "Le mot de passe est requis",
            'minlength': "Le mot de passe doit être de 6 caractères minimum",
        },
        'cpassword': {
            'confirm': "Le mot de passe ne correspond pas",
        }
    };

    prenom: string = '';
    nom: string = '';
    phone: string = '';
    password: string = '';
    email: string = '';
    cpassword: string = '';
    errorPassword = '';
    errorPhone = '';
    errorEmail = '';
    sending: boolean = false;
    accepteConditions: boolean = false;

    constructor(
        public router: Router,
        public formBuilder: FormBuilder,
        public angularFireAuth: AngularFireAuth,
        private alertController: AlertController,
        private popoverController: PopoverController,
        public userService: UserService,
        public countriesService: CountriesService,
        private platform: Platform
    ) {
        this.registerForm = this.formBuilder.group({
            login: ['', [Validators.required]],
            email: ['', [Validators.required, Validators.email]],
            password: ['', [Validators.minLength(6)]],
            cpassword: ['']
        });

        this.registerForm.valueChanges.subscribe(data => this.onValueChanged(data));
        this.onValueChanged(); // reset validation messages
    }

    ngOnInit() {
    }

    async ionViewDidEnter() {
        this.userService.recaptchaVerifier = new firebase.auth.RecaptchaVerifier('sign-in-button', {
            size: 'invisible',
            callback: (response) => {

            },
            'expired-callback': () => {
            }
        });
    }

    ionViewDidLoad() {
        this.userService.recaptchaVerifier = new firebase.auth.RecaptchaVerifier('sign-in-button', {
            size: 'invisible',
            callback: (response) => {

            },
            'expired-callback': () => {
            }
        });
    }

    async register() {
        if (this.cpassword && this.password !== this.cpassword) {
            return this.errorPassword = "Le mot de passe ne correspond pas";
        }
        try {
            this.userService.presentLoading("Bienvenue chez CookDay");
            const db = firebase.firestore();
            let self = this;
            if (this.platform.is('ios')) {
                this.angularFireAuth.createUserWithEmailAndPassword(this.email, this.password).then(function (cred) {
                    return db.collection('utilisateurs').doc(cred.user.uid).set({
                        dateCreation: firebase.firestore.Timestamp.fromDate(new Date()),
                        commandes: [],
                        nom: self.nom,
                        prenom: self.prenom,
                        notifs: [],
                        phone: {
                            callingCode: self.countriesService.selectedCountry.dial_code[0],
                            flag: 'https://cdn.countryflags.com/thumbs/' + (self.countriesService.selectedCountry.name.toLowerCase()) + '/flag-3d-round-250.png',
                            num: self.phone
                        }
                    });
                }).then(() => {
                        self.userService.SendVerificationMail()
                            .then(() => self.router.navigate(['login']))
                            .then(() => self.userService.loadingController.dismiss());
                    }
                ).catch(error => {
                    console.log(error);
                    this.userService.loadingController.dismiss();
                    if (error.code == 'auth/email-already-in-use') {
                        this.errorEmail = "Un compte utilise déjà ce mail."
                    }
                });
            } else {
                this.signInWithPhoneNumber(this.userService.recaptchaVerifier, this.countriesService.selectedCountry.dial_code + this.phone)
                    .then(success => {
                        this.angularFireAuth.createUserWithEmailAndPassword(this.email, this.password).then(function (cred) {
                            return db.collection('utilisateurs').doc(cred.user.uid).set({
                                dateCreation: firebase.firestore.Timestamp.fromDate(new Date()),
                                commandes: [],
                                nom: self.nom,
                                prenom: self.prenom,
                                notifs: [],
                                phone: {
                                    callingCode: self.countriesService.selectedCountry.dial_code[0],
                                    flag: 'https://cdn.countryflags.com/thumbs/' + (self.countriesService.selectedCountry.name.toLowerCase()) + '/flag-3d-round-250.png',
                                    num: self.phone
                                }
                            });
                        }).then(() => {
                                let navigationExtras: NavigationExtras = {
                                    queryParams: {
                                        email: JSON.stringify(this.email),
                                        dial_code: JSON.stringify(this.countriesService.selectedCountry.dial_code),
                                        phone: JSON.stringify(this.phone)
                                    }
                                };
                                this.router.navigate(['confirm-phone'], navigationExtras).then(() => {
                                    this.sending = false;
                                    this.userService.loadingController.dismiss();
                                });
                            }
                        ).catch(error => {
                            console.log(error);
                            this.userService.loadingController.dismiss();
                            if (error.code == 'auth/email-already-in-use') {
                                this.errorEmail = "Un compte utilise déjà ce mail."
                            }
                        });
                    })
                    .catch(error => {
                        console.log(error);
                        this.userService.loadingController.dismiss();
                        this.errorPhone = "Numéro de téléphone invalide"
                    });
            }
        } catch (error) {
            this.errorEmail = "Email non valide ou déja utilisé";
            console.dir(error);
            this.sending = false;
        }
    }

    goTologin() {
        this.router.navigate(['login']);
    }

    onValueChanged(data ?: any) {
        if (!this.registerForm) {
            return;
        }
        const form = this.registerForm;
        for (const field in this.formErrors) {
            // clear previous error message (if any)
            this.formErrors[field] = [];
            this.errorPassword = '';
            this.errorEmail = '';
            const control = form.get(field);
            if (control && control.dirty && !control.valid) {
                const messages = this.validationMessages[field];
                for (const key in control.errors) {
                    this.formErrors[field].push(messages[key]);
                }
            }
        }
    }

    validForm() {
        return !this.prenom || !this.nom || !this.email || this.phone == "" || !this.password || !this.cpassword || !this.accepteConditions;
    }

    async openSelectCountry(ev: any) {
        const popover = await this.popoverController.create({
            component: SelectCountryComponent,
            event: ev,
            translucent: true,
            cssClass: 'popover_class'
        });
        return await popover.present();
    }

    public signInWithPhoneNumber(recaptchaVerifier, phoneNumber) {
        return new Promise<any>((resolve, reject) => {

            this.angularFireAuth.signInWithPhoneNumber(phoneNumber, recaptchaVerifier)
                .then((confirmationResult) => {
                    this.userService.confirmationResult = confirmationResult;
                    resolve(confirmationResult);
                }).catch((error) => {
                console.log(error);
                reject('SMS not sent');
            });
        });
    }

    openTerms(){
        window.open(
            'https://firebasestorage.googleapis.com/v0/b/cookday-9366d.appspot.com/o/CGU%20CookDay.pdf?alt=media&token=f9886f24-98bd-4566-882a-891243ccec22', '_blank')
    }

}
