import {Component, OnInit, ViewChild} from '@angular/core';
import {AlertController, ViewDidEnter} from "@ionic/angular";
import {UserService} from "../../services/user.service";
import {ActivatedRoute, Router} from "@angular/router";
import {AngularFireAuth} from "@angular/fire/auth";
import {AngularFirestore} from "@angular/fire/firestore";

@Component({
    selector: 'app-confirm-phone',
    templateUrl: './confirm-phone.page.html',
    styleUrls: ['./confirm-phone.page.scss'],
})
export class ConfirmPhonePage implements OnInit, ViewDidEnter {

    @ViewChild('input1') myInput1;
    @ViewChild('input2') myInput2;
    @ViewChild('input3') myInput3;
    @ViewChild('input4') myInput4;
    @ViewChild('input5') myInput5;
    @ViewChild('input6') myInput6;

    code = ["", "", "", "", "", ""];
    inputs: any;
    showIncorrect: boolean = false;
    email: '';
    phone: '';
    dial_code: '';

    constructor(
        private userService: UserService,
        private alertController: AlertController,
        public router: Router,
        private angularFireAuth: AngularFireAuth,
        private route: ActivatedRoute,
        private readonly angularFirestore: AngularFirestore
    ) {
        this.route.queryParams.subscribe(params => {
            if (params) {
                this.email = params.email ? JSON.parse(params.email) : null;
                this.dial_code = JSON.parse(params.dial_code);
                this.phone = JSON.parse(params.phone);
            }
        });
    }

    ngOnInit() {
    }

    ionViewDidEnter() {
        this.focus();
        this.inputs = [this.myInput1, this.myInput2, this.myInput3, this.myInput4]
    }

    onInput() {
        this.showIncorrect = false;
        let self = this;
        setTimeout(function () {
            self.focus();
        }, 50);
    }

    shake(name) {
        let inputCard = document.querySelector("." + name) as HTMLElement;
        inputCard.style.animation = "shake 0.82s cubic-bezier(.36,.07,.19,.97) both";
        setTimeout(function () {
            inputCard.style.animation = "";
        }, 820);
    }

    focus() {
        if (this.code[0] == "") {
            this.myInput1.setFocus();
        } else {
            if (this.code[1] == "") {
                this.myInput2.setFocus();
            } else {
                if (this.code[2] == "") {
                    this.myInput3.setFocus();
                } else {
                    if (this.code[3] == "") {
                        this.myInput4.setFocus();
                    } else {
                        if (this.code[4] == "") {
                            this.myInput5.setFocus();
                        } else {
                            this.myInput6.setFocus();
                        }
                    }
                }
            }
        }
    }

    renvoyerCode() {
        return new Promise<any>((resolve, reject) => {

            this.angularFireAuth.signInWithPhoneNumber(this.dial_code+this.phone, this.userService.recaptchaVerifier)
                .then((confirmationResult) => {
                    this.userService.confirmationResult = confirmationResult;
                    resolve(confirmationResult);
                    this.userService.toast("Le code de validation a été renvoyé.", 2000);
                }).catch((error) => {
                console.log(error);
                reject('SMS not sent');
            });
        });
    }

    async alertCodeConfirme() {
        const alert = await this.alertController.create({
            header: "Code confirmé",
            message: "Votre numéro de téléphone a été validé.",
            buttons: [
                {
                    text: "Compris",
                    role: 'cancel',
                    cssClass: 'primary',
                    handler: (blah) => {
                    }
                },
            ]
        });
        await alert.present();
    }

    public async control() {
        this.userService.presentLoading("As tu tiré le bon numéro?").then(() => {
            let self = this;
            setTimeout(function () {
                return new Promise<any>((resolve, reject) => {
                    let res = "";
                    self.code.forEach(e => {
                        res += e;
                    })
                    self.userService.confirmationResult.confirm(res).then(async (result) => {
                        if(self.email){
                            const user = result.user;
                            await self.userService.SendVerificationMail()
                                .then(() => self.router.navigate(['login']))
                                .then(() => self.userService.loadingController.dismiss())
                                .then(()=> resolve(user));
                        }
                        else{
                            self.userService.user.phone.callingCode = self.dial_code;
                            self.userService.user.phone.num = self.phone;
                            self.angularFirestore.doc("utilisateurs/"+self.userService.userInfo.uid).update({
                                phone: self.userService.user.phone
                            }).then(() => self.router.navigate(['principale']))
                                .then(() => self.userService.loadingController.dismiss())
                                .then(() => resolve(self.userService.toast("Votre numéro de téléphone a bien été modifié.", 2000)));
                        }
                    }).catch((error) => {
                        reject(error.message);
                        self.code = ["", "", "", "", "", ""];
                        self.showIncorrect = true;
                        self.shake("code");
                        self.myInput1.setFocus();
                        self.userService.loadingController.dismiss();
                    });
                });
            }, 100);
        })
    }

}
