import {ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {AlertController, NavController, Platform} from "@ionic/angular";
import {VentesService} from "../../services/ventes.service";
import {ActivatedRoute, Router} from "@angular/router";
import {UserService} from "../../services/user.service";

@Component({
    selector: 'app-vente',
    templateUrl: './vente.page.html',
    styleUrls: ['./vente.page.scss'],
})
export class VentePage implements OnInit {

    id: string;

    constructor(
        private navController: NavController,
        public ventesService: VentesService,
        private route: ActivatedRoute,
        public userService: UserService,
        private alertController: AlertController,
        public platform: Platform,
        private router: Router,
        private cd: ChangeDetectorRef,
    ) {
        this.route.queryParams.subscribe(params => {
            if (params && params.id) {
                this.id = JSON.parse(params.id);
            }
        });
    }

    ngOnInit() {
    }

    back() {
        this.navController.pop();
    }

    vente() {
        return this.ventesService.ventes[this.id];
    }

    isToday() {
        var todayDate = new Date();
        return this.vente().date.getFullYear() == todayDate.getFullYear()
            && this.vente().date.getMonth() == todayDate.getMonth()
            && this.vente().date.getDate() == todayDate.getDate();
    }

    isTomorrow() {
        var todayDate = new Date();
        return this.vente().date.getFullYear() == todayDate.getFullYear()
            && this.vente().date.getMonth() == todayDate.getMonth()
            && this.vente().date.getDate() == todayDate.getDate() + 1;
    }

    removeQuantite(plat){
        this.userService.user ? (plat.quantiteAAjouter>0 ? plat.quantiteAAjouter-- : null) :  this.alertLogin();
        return this.cd.detectChanges();
    }

    async alertLogin() {
        const alert = await this.alertController.create({
            header: "Connexion requise",
            message: "Seules les utilisateurs connectés peuvent commander sur CookDay!",
            buttons: [
                {
                    text: "Se connecter",
                    cssClass: 'color-danger',
                    handler: (blah) => {
                        this.router.navigate(['login']);
                    }
                },
            ]
        });
        await alert.present();
    }

    minusOpacity(plat){
        return plat.quantiteAAjouter>0 ? '1' : '0.4';
    }

    maxOpacity(plat){
        return this.qteReste(plat) ? '1' : '0.4';
    }

    qteReste(plat){
        return this.userService.user ? (plat.quantite-(plat.qteAchetee+plat.quantiteAAjouter+this.ventesService.getQtePanier(plat)))>0 : 1;
    }

    addQuantite(plat){
        this.userService.user ? (this.qteReste(plat) ? plat.quantiteAAjouter++ : null) :  this.alertLogin();
        return this.cd.detectChanges();
    }

    addPlat(cat, plat){
        if(this.userService.user){
            if(!this.userService.user.panier){
                this.startNewPanier(cat, plat);
            }
            else if(this.userService.user.panier.vente==this.id){
                let cible = this.userService.user.panier.menu[this.ventesService.retirerAccents(cat)].find(p => p.nom == plat.nom);
                if(cible){
                    cible.quantite += plat.quantiteAAjouter;
                }
                else{
                    let newPlat = {
                        img: plat.img,
                        ingredients: plat.ingredients,
                        nom: plat.nom,
                        prix: plat.prix,
                        quantite: plat.quantiteAAjouter
                    } as any;
                    this.userService.user.panier.menu[this.ventesService.retirerAccents(cat)].push(newPlat);
                }
                plat.quantiteAAjouter = 0;
                this.userService.updatePanier();
                this.userService.toast(plat.nom + " a été ajouté au panier", 2000);
            }
            else{
                this.alertChangementCuisine(cat, plat);
            }
        }
        else{
            this.alertLogin();
        }
    }

    startNewPanier(cat, plat){
        let newPanier = {
            cuisine: this.vente().cuisine,
            vente: this.id,
            menu: {
                entrees: [],
                plats: [],
                desserts: [],
                boissons: [],
            },
            ajoutCouvertsServiettes: false
        }
        /*let newPlat = {} as any;
        Object.assign(newPlat, plat);
        newPlat.quantite = plat.quantiteAAjouter;
        newPanier.menu[this.ventesService.retirerAccents(cat)].push(plat);*/
        this.userService.user.panier = newPanier;
        this.addPlat(cat, plat);
        /*plat.quantiteAAjouter = 0;
        this.userService.updatePanier();
        this.userService.toast(plat.nom + " a été ajouté au panier", 2000);*/
    }

    async alertChangementCuisine(cat, plat) {
        const alert = await this.alertController.create({
            header: "Changement de cuisine",
            message: "Vous avez déjà un panier en cours pour une autre vente. Voulez vous supprimer votre précédent panier et en commencer un nouveau?",
            buttons: [
                {
                    text: 'Annuler',
                    role: 'cancel',
                    cssClass: 'color-medium',
                    handler: (blah) => {
                    }
                },
                {
                    text: 'Confirmer',
                    cssClass: 'color-first',
                    handler: () => {
                        this.startNewPanier(cat, plat);
                    }
                }
            ]
        });

        await alert.present();
    }


}
