import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { NoterCommandePageRoutingModule } from './noter-commande-routing.module';

import { NoterCommandePage } from './noter-commande.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    NoterCommandePageRoutingModule
  ],
  declarations: [NoterCommandePage]
})
export class NoterCommandePageModule {}
