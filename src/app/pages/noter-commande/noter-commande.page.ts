import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, NavigationExtras, Router} from "@angular/router";
import {NavController, PopoverController} from "@ionic/angular";
import {UserService} from "../../services/user.service";
import {VentesService} from "../../services/ventes.service";
import {SignalerComponent} from "../../components/signaler/signaler.component";

@Component({
    selector: 'app-noter-commande',
    templateUrl: './noter-commande.page.html',
    styleUrls: ['./noter-commande.page.scss'],
})
export class NoterCommandePage implements OnInit {

    id: string;
    note: number = 0;
    remarque: string = "";

    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private navController: NavController,
        private userService: UserService,
        public ventesService: VentesService,
        private popoverController: PopoverController,
    ) {
        this.route.queryParams.subscribe(params => {
            if (params && params.id) {
                this.id = JSON.parse(params.id);
            }
        });
    }

    ngOnInit() {
    }

    back() {
        this.navController.pop();
    }

    commande() {
        return this.userService.commandesData[this.id];
    }

    cuisine() {
        return this.ventesService.getCuisine(this.commande());
    }

    async signaler() {
        let navigationExtras: NavigationExtras = {
            queryParams: {
                id: JSON.stringify(this.id)
            }
        };
        return this.router.navigate(['signaler'], navigationExtras);
    }

    async presentAlertSignaler() {
        const popover = await this.popoverController.create({
            component: SignalerComponent,
            translucent: true,
            componentProps: {
                id: this.id
            }
        });
        return await popover.present();
    }

    noter(){
        this.ventesService.noterCommande(this.id, this.note, this.remarque);
    }

}
