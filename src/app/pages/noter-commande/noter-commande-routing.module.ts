import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { NoterCommandePage } from './noter-commande.page';

const routes: Routes = [
  {
    path: '',
    component: NoterCommandePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class NoterCommandePageRoutingModule {}
