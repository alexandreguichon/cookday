import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AjouterPlatCuisinePage } from './ajouter-plat-cuisine.page';

const routes: Routes = [
  {
    path: '',
    component: AjouterPlatCuisinePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AjouterPlatCuisinePageRoutingModule {}
