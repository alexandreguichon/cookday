import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AjouterPlatCuisinePageRoutingModule } from './ajouter-plat-cuisine-routing.module';

import { AjouterPlatCuisinePage } from './ajouter-plat-cuisine.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AjouterPlatCuisinePageRoutingModule
  ],
  declarations: [AjouterPlatCuisinePage]
})
export class AjouterPlatCuisinePageModule {}
