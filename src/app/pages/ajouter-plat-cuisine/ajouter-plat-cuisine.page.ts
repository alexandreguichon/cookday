import {ChangeDetectorRef, Component, OnInit, ViewRef} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {ActionSheetController, AlertController, NavController, Platform} from "@ionic/angular";
import {VentesService} from "../../services/ventes.service";
import {Camera, CameraResultType, CameraSource} from "@capacitor/camera";
import {Camera as CameraCordova, CameraOptions} from "@ionic-native/camera/ngx";
import firebase from 'firebase/app';
import {UserService} from "../../services/user.service";

@Component({
    selector: 'app-ajouter-plat-cuisine',
    templateUrl: './ajouter-plat-cuisine.page.html',
    styleUrls: ['./ajouter-plat-cuisine.page.scss'],
})
export class AjouterPlatCuisinePage implements OnInit {

    db = firebase.firestore();

    cat: string = "";
    previousPlat: any = null;
    plat: any = null;
    editing: boolean = false;
    editedPhoto: boolean = false;
    nom: string = "";
    newIngredient: string = "";
    ingredients: string[] = [];
    prix: number;
    photo = null;

    constructor(
        private route: ActivatedRoute,
        private navController: NavController,
        public ventesService: VentesService,
        public cd: ChangeDetectorRef,
        private alertController: AlertController,
        public actionsheetCtrl: ActionSheetController,
        private cameraCordova: CameraCordova,
        public userService: UserService,
        private router: Router,
        private readonly actionSheetController: ActionSheetController,
        public readonly platform: Platform,
    ) {
        this.route.queryParams.subscribe(params => {
            if (params && params.cat) {
                if (params.cat) {
                    this.cat = JSON.parse(params.cat);
                }
                if (params.plat) {
                    this.previousPlat = JSON.parse(params.plat);
                    this.plat = JSON.parse(params.plat);
                    this.plat["cat"] = this.cat;
                    this.editing = true;
                    this.nom = this.plat.nom;
                    this.ingredients = this.plat.ingredients;
                    this.photo = {
                        img: this.plat.img,
                        clicked: false
                    };
                    this.prix = this.plat.prix
                }
            }
        });
    }

    ngOnInit() {
    }

    async back() {
        return this.navController.pop();
    }

    ajouterIngredient() {
        if (this.newIngredient.length > 0) {
            this.ingredients.push(this.newIngredient);
            this.newIngredient = "";
        }
    }

    deleteIngredient(index) {
        this.ingredients.splice(index, 1);
    }

    async showDeletePhotoAlert() {
        const alert = await this.alertController.create({
            header: "Supprimer la photo",
            message: "Es-tu sûr de vouloir supprimer la photo?",
            buttons: [
                {
                    text: "Annuler",
                    role: 'cancel',
                    handler: (blah) => {
                        this.photo.clicked = false;
                    }
                },
                {
                    text: "Confirmer",
                    handler: () => {
                        this.photo = null;
                    }
                }
            ]
        });

        await alert.present();
    }

    detectChanges() {
        if (this.cd && !(this.cd as ViewRef).destroyed) {
            this.cd.detectChanges();
        }
    }

    async openPhotoChoice() {
        if (this.platform.is('capacitor')) {
            const actionSheet = await this.actionSheetController.create({
                cssClass: 'action-sheets-basic-page',
                buttons: [
                    {
                        text: 'Prendre une photo',
                        role: 'destructive',
                        icon: 'camera-outline',
                        handler: () => this.getImageFromSource(CameraSource.Camera)
                    },
                    {
                        text: 'Choisir dans la galerie',
                        icon: 'images-outline',
                        handler: () => this.getImageFromSource(CameraSource.Photos)
                    },
                ]
            });
            await actionSheet.present();
        } else {
            this.getImageFromSource(CameraSource.Photos);
        }
    }

    private getImageFromSource(source: CameraSource): void {
        Camera.getPhoto({
            resultType: CameraResultType.Uri,
            source,
        }).then((pic) => {
        console.log(pic);
            this.photo = {
                img: pic.webPath,
                path: 'data:image/jpeg;base64,' + pic.webPath,
                clicked: false
            };
            this.editedPhoto = true;
        });
    }

    btnDisabled() {
        return this.nom == '' || this.ingredients.length == 0 || !this.photo || !this.prix;
    }

    addPlat() {
        if (this.editing) {
            this.ventesService.presentLoading("Modification du produit").then(() => {
                return new Promise<any>((resolve, reject) => {
                    if (this.editedPhoto) {
                        let storageRef = firebase.storage().ref();
                        let imageRef = storageRef.child(this.userService.userInfo.uid).child('Plats').child(this.nom + '.jpg');
                        let self = this;
                        this.encodeImageUri(this.photo.img, function (image64) {
                            imageRef.putString(image64, 'data_url')
                                .then(snapshot => {
                                    imageRef.getDownloadURL().then((lienImg) => {
                                        self.ventesService.maCuisine().menu[self.ventesService.retirerAccents(self.plat.cat)] = self.ventesService.maCuisine().menu[self.ventesService.retirerAccents(self.plat.cat)].filter(e => e.nom != self.plat.nom && e.ingredients != self.plat.ingredients);
                                        self.ventesService.maCuisine().menu[self.ventesService.retirerAccents(self.cat)].push({
                                            img: lienImg,
                                            ingredients: self.ingredients,
                                            nom: self.nom,
                                            prix: self.prix
                                        });
                                        self.db.collection('cuisines').doc(self.userService.userInfo.uid).update(self.ventesService.maCuisine()).then(function () {
                                            self.router.navigate(['principale']).then(() => {
                                                self.userService.loadingController.dismiss().then(() => {
                                                    self.userService.toast('Les modifications ont été enregistrées', 2000);
                                                })
                                            });
                                        });
                                    });
                                }, err => {
                                    reject(err);
                                });
                        });
                    } else {
                        this.ventesService.maCuisine().menu[this.ventesService.retirerAccents(this.plat.cat)] = this.ventesService.maCuisine().menu[this.ventesService.retirerAccents(this.plat.cat)].filter(e => e.nom != this.plat.nom && e.ingredients != this.plat.ingredients);
                        this.ventesService.maCuisine().menu[this.ventesService.retirerAccents(this.cat)].push({
                            img: this.plat.img,
                            ingredients: this.ingredients,
                            nom: this.nom,
                            prix: this.prix
                        });
                        let self = this;
                        this.db.collection('cuisines').doc(this.userService.userInfo.uid).update(this.ventesService.maCuisine()).then(function () {
                            self.router.navigate(['principale']).then(() => {
                                self.userService.loadingController.dismiss().then(() => {
                                    self.userService.toast('Les modifications ont été enregistrées', 2000);
                                })
                            });
                        });
                    }
                });
            });
        } else {
            this.ventesService.presentLoading("Ajout du produit au menu").then(() => {
                return new Promise<any>((resolve, reject) => {
                    let storageRef = firebase.storage().ref();
                    let imageRef = storageRef.child(this.userService.userInfo.uid).child('Plats').child(this.nom + '.jpg');
                    let self = this;
                    this.encodeImageUri(this.photo.img, function (image64) {
                        imageRef.putString(image64, 'data_url')
                            .then(snapshot => {
                                const db = firebase.firestore();
                                imageRef.getDownloadURL().then((lienImg) => {
                                    self.ventesService.maCuisine().menu[self.ventesService.retirerAccents(self.cat)].push({
                                        img: lienImg,
                                        ingredients: self.ingredients,
                                        nom: self.nom,
                                        prix: self.prix
                                    });
                                    db.collection('cuisines').doc(self.userService.userInfo.uid).update(self.ventesService.maCuisine()).then(function () {
                                        self.router.navigate(['principale']).then(() => {
                                            self.userService.loadingController.dismiss().then(() => {
                                                self.userService.toast('Félicitations! Aux fourneaux!', 2000);
                                            })
                                        });
                                    });
                                });
                            }, err => {
                                reject(err);
                            });
                    });
                });
            });
        }
    }

    encodeImageUri(imageUri, callback) {
        var c = document.createElement('canvas');
        var ctx = c.getContext('2d');
        var img = new Image();
        img.onload = function () {
            var aux: any = this;
            c.width = aux.width;
            c.height = aux.height;
            ctx.drawImage(img, 0, 0);
            var dataURL = c.toDataURL('image/jpeg');
            callback(dataURL);
        };
        img.src = imageUri;
    };

    async presentDeleteAlert() {
        const alert = await this.alertController.create({
            header: "Supprimer " + this.plat.nom,
            message: this.plat.nom + " va être retiré des plats proposés. Tu confirmes ton action?",
            buttons: [
                {
                    text: 'Annuler',
                    role: 'cancel',
                    cssClass: 'secondary',
                    handler: (blah) => {
                    }
                },
                {
                    text: 'Confirmer',
                    handler: () => {
                        this.deletePlat();
                    }
                }
            ]
        });

        await alert.present();
    }

    deletePlat() {
        this.ventesService.maCuisine().menu[this.ventesService.retirerAccents(this.cat)] = this.ventesService.maCuisine().menu[this.ventesService.retirerAccents(this.cat)].filter(e => e.nom != this.previousPlat.nom && e.prix != this.previousPlat.prix);
        this.back().then(() => {
            this.userService.toast(this.plat.nom + " a été retiré du menu.", 2000);
            this.db.collection('cuisines').doc(this.ventesService.maCuisine().id).update({
                menu: this.ventesService.maCuisine().menu
            });
        });
    }

    onAutoFormatChanged() {
        this.nom = this.setFirstLetterToUppercase(this.nom);
        this.cd.detectChanges();
    }

    onAutoFormatChangedIngredient() {
        this.newIngredient = this.setFirstLetterToUppercase(this.newIngredient);
        this.cd.detectChanges();
    }

    setFirstLetterToUppercase(string:string):string {
        return string.charAt(0).toUpperCase() + string.slice(1);
    }
}
