import {Component, OnInit} from '@angular/core';
import {NavController, Platform} from '@ionic/angular';
import { App } from '@capacitor/app';

@Component({
    selector: 'app-a-propos',
    templateUrl: './a-propos.page.html',
    styleUrls: ['./a-propos.page.scss'],
})
export class AProposPage implements OnInit {

    actualVersion: string = "";

    constructor(
        private platform: Platform,
        private navController: NavController,
    ) {
        App.getInfo().then(res => this.actualVersion = res.version);
    }

    ngOnInit() {
    }

    openOtherApps() {
        window.open('https://play.google.com/store/apps/dev?id=7965824296013501583',
            '_system');
        return false;
    }


    openWhaleX() {
        window.open('https://website.whalex.fr/',
            '_system');
        return false;
    }

    openCompso() {
        window.open('https://compso.myportfolio.com/',
            '_system');
        return false;
    }

    back() {
        this.navController.pop();
    }

}
