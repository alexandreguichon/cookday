import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {NavController, Platform} from "@ionic/angular";
import {VentesService} from "../../services/ventes.service";
import {UserService} from "../../services/user.service";
import firebase from 'firebase/app';

@Component({
    selector: 'app-commande',
    templateUrl: './commande.page.html',
    styleUrls: ['./commande.page.scss'],
})
export class CommandePage implements OnInit {

    id: string;
    nbMessagesNonLus: number = 0;

    constructor(
        private route: ActivatedRoute,
        private navController: NavController,
        public ventesService: VentesService,
        public userService: UserService,
        public platform: Platform
    ) {
        this.route.queryParams.subscribe(params => {
            if (params && params.id) {
                this.id = JSON.parse(params.id);
            }
        });
    }

    ngOnInit() {
        firebase.database().ref('chat/' + this.userService.userInfo.uid + '/' + this.commande().cuisine + '/').on('value', resp => {
                let chatData = this.snapshotToArray(resp);
                this.nbMessagesNonLus = chatData.filter(message => (message.auteur != this.userService.userInfo.uid) && (!message.lu)).length;
            }
        );
    }

    snapshotToArray(snapshot: any) {
        let returnArray = [];
        snapshot.forEach(element => {
            let item = element.val();
            item.key = element.key;
            item.date = new Date(item.date);
            returnArray.push(item);
        });
        return returnArray;
    }

    back() {
        this.navController.pop();
    }

    commande() {
        return this.userService.commandesData[this.id];
    }

    cuisine() {
        return this.ventesService.getCuisine(this.commande());
    }

    statutColor() {
        return this.commande().statut == 'terminée' ? 'var(--ion-color-success)' : 'var(--ion-color-danger)';
    }

}
