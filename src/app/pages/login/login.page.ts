import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {SubscriptionLike as ISubscription} from 'rxjs';
import {AlertController} from "@ionic/angular";
import {Router} from "@angular/router";
import {AngularFireAuth} from "@angular/fire/auth";
import {UserService} from "../../services/user.service";
/*import {AngularFireAuth} from "@angular/fire/auth";
import * as firebase from "firebase/app";*/

@Component({
    selector: 'app-login',
    templateUrl: './login.page.html',
    styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

    private subscriptionArray: Array<ISubscription> = [];
    private subscription: ISubscription;

    loginForm: FormGroup;
    formErrors = {
        'email': '',
        'password': ''
    };
    validationMessages = {
        'email': {
            'required': "Une adresse email est requise",
            'email': "L'adresse email n'est pas valide"
        },
        'password': {
            'required': "Le mot de passe est requis"
        }
    };
    errorPassword = '';
    errorEmail = '';
    connecting: boolean = false;

    constructor(
        public router: Router,
        public formBuilder: FormBuilder,
        private alertController: AlertController,
        public forgotCtrl: AlertController,
        public angularFireAuth: AngularFireAuth,
        private userService: UserService
    ) {
        this.loginForm = this.formBuilder.group({
            email: ['', [Validators.required, Validators.email]],
            password: ['',
                [Validators.required]
            ],
        });
        this.subscriptionArray.push(this.loginForm.valueChanges.subscribe(data => this.onValueChanged(data)));
        this.onValueChanged(); // reset validation messages
    }

    ngOnInit() {
    }

    onValueChanged(data ?: any) {
        if (!this.loginForm) {
            return;
        }
        const form = this.loginForm;
        for (const field in this.formErrors) {
            // clear previous error message (if any)
            this.formErrors[field] = [];
            this.errorPassword = '';
            this.errorEmail = '';
            const control = form.get(field);
            if (control && control.dirty && !control.valid) {
                const messages = this.validationMessages[field];
                for (const key in control.errors) {
                    this.formErrors[field].push(messages[key]);
                }
            }
        }
    }

    async forgotPass() {
        let self = this;
        const forgot = await this.forgotCtrl.create({
            header: "Mot de passe oublié ?",
            message: "Entrez votre adresse mail pour recevoir un lien de reset de mot de passe.",
            inputs: [
                {
                    name: 'email',
                    placeholder: 'Email',
                    type: 'email'
                },
            ],
            buttons: [
                {
                    text: "Annuler",
                    handler: data => {
                        console.log('Cancel clicked');
                    }
                },
                {
                    text: "Envoyer",
                    handler: async data => {
                        this.angularFireAuth.sendPasswordResetEmail(data.email).then(async function () {
                            const toast = await self.userService.toastCtrl.create({
                                message: "L'email a été envoyé avec succès.",
                                duration: 3000,
                                position: 'top',
                                cssClass: 'dark-trans'
                            });
                            await toast.present();
                        })
                            .catch(async function (error) {
                                if (error.code == 'auth/invalid-email') {
                                    console.log(error);
                                    const alert = await self.alertController.create({
                                        header: "Email invalide",
                                        message: "L'adresse email n'est pas valide.",
                                        buttons: [
                                            {
                                                text: 'Ok',
                                                role: 'cancel',
                                                cssClass: 'secondary',
                                                handler: (blah) => {
                                                }
                                            }
                                        ]
                                    });
                                    await alert.present();
                                }
                            });
                    }
                }
            ]
        });
        forgot.present();
    }

    async login() {
        if(!this.userService.user){
            this.userService.presentLoading("Connexion en cours").then(()=>{
                this.connecting = true;
                return this.angularFireAuth.signInWithEmailAndPassword(this.loginForm.get('email').value, this.loginForm.get('password').value)
                    .then((result) => {
                        if (result.user.emailVerified != true) {
                            this.userService.loadingController.dismiss();
                            this.userService.SendVerificationMail();
                            this.connecting = false;
                            window.alert("Vous n'avez pas encore validé votre adresse mail. Allez vérifier votre boîte mail!");
                        } else {
                            this.userService.userInfo = result.user;
                            this.userService.getData().then(()=>{
                                this.connecting = false;
                                // this.router.navigate(['principale']).then(()=>{
                                //     this.userService.loadingController.dismiss();
                                // });
                            })
                        }
                    }).catch((error) => {
                        this.userService.loadingController.dismiss();
                        this.connecting = false;
                        this.errorPassword = "Mot de passe incorrect";
                        console.log(this.errorPassword);
                    });
            });
        }
    }

    goToRegister() {
        this.router.navigate(['register']);
    }

    goToPrincipale() {
        this.router.navigate(['principale']);
    }

}
