import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { EvaluationsCuisinePageRoutingModule } from './evaluations-cuisine-routing.module';

import { EvaluationsCuisinePage } from './evaluations-cuisine.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    EvaluationsCuisinePageRoutingModule
  ],
  declarations: [EvaluationsCuisinePage]
})
export class EvaluationsCuisinePageModule {}
