import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { EvaluationsCuisinePage } from './evaluations-cuisine.page';

const routes: Routes = [
  {
    path: '',
    component: EvaluationsCuisinePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class EvaluationsCuisinePageRoutingModule {}
