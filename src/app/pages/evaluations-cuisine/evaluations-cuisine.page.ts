import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from "@angular/router";

@Component({
    selector: 'app-evaluations-cuisine',
    templateUrl: './evaluations-cuisine.page.html',
    styleUrls: ['./evaluations-cuisine.page.scss'],
})
export class EvaluationsCuisinePage implements OnInit {

    id: string;

    constructor(
        private route: ActivatedRoute,
    ) {
        this.route.queryParams.subscribe(params => {
            if (params && params.id) {
                this.id = JSON.parse(params.id);
            }
        });
    }

    ngOnInit() {
    }

}
