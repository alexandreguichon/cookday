import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DevenirCuisinePage } from './devenir-cuisine.page';

const routes: Routes = [
  {
    path: '',
    component: DevenirCuisinePage
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DevenirCuisinePageRoutingModule {}
