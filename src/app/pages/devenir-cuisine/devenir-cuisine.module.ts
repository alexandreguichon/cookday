import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DevenirCuisinePageRoutingModule } from './devenir-cuisine-routing.module';

import { DevenirCuisinePage } from './devenir-cuisine.page';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        DevenirCuisinePageRoutingModule
    ],
    declarations: [
        DevenirCuisinePage,
    ]
})
export class DevenirCuisinePageModule {}
