import {ChangeDetectorRef, Component, NgZone, OnInit, SecurityContext, ViewChild, ViewRef} from '@angular/core';
import {ActionSheetController, AlertController, IonInput, IonTextarea, NavController, Platform} from "@ionic/angular";
import {Camera as CameraCordova, CameraOptions} from '@ionic-native/camera/ngx';
import {File} from '@ionic-native/file/ngx';
import {NavigationExtras, Router} from "@angular/router";
import {Camera, CameraResultType, CameraSource} from '@capacitor/camera';
import firebase from 'firebase/app';
import {UserService} from "../../services/user.service";
import { DomSanitizer } from '@angular/platform-browser';

@Component({
    selector: 'app-devenir-cuisine',
    templateUrl: './devenir-cuisine.page.html',
    styleUrls: ['./devenir-cuisine.page.scss'],
})
export class DevenirCuisinePage implements OnInit {

    constructor(
        private navController: NavController,
        private alertController: AlertController,
        private cd: ChangeDetectorRef,
        public actionsheetCtrl: ActionSheetController,
        private cameraCordova: CameraCordova,
        private file: File,
        private router: Router,
        public userService: UserService,
        public zone: NgZone,
        public platform: Platform,
        public domSanitizer: DomSanitizer,
        private readonly actionSheetController: ActionSheetController,
    ) {
    }

    @ViewChild('input') myInput: IonInput;
    @ViewChild('textarea') myDescription: IonTextarea;

    GooglePlaces: google.maps.places.PlacesService;
    GoogleAutocomplete: google.maps.places.AutocompleteService;
    0.
    autocomplete: { input: string; };
    autocompleteItems: any[];
    inputFocused: boolean = false;

    nom: string = '';
    adresse: string = '';
    description: string = '';
    selectedPlaceId: string = '';

    myPhotos = [/*{
        img: 'assets/Google.png',
        clicked: false
    }*/];

    ngOnInit() {
        this.GoogleAutocomplete = new google.maps.places.AutocompleteService();
        this.autocompleteItems = [];
        if (this.adresse && this.adresse != '') {
            this.autocomplete = {input: this.adresse};
            this.updateSearchResults();
        } else {
            this.autocomplete = {input: ''};
        }
    }

    updateSearchResults() {
        if (this.autocomplete.input == '') {
            this.autocompleteItems = [];
            return;
        }
        this.GoogleAutocomplete.getPlacePredictions({input: this.autocomplete.input},
            (predictions, status) => {
                this.autocompleteItems = [];
                this.zone.run(() => {
                    predictions.forEach((prediction) => {
                        this.autocompleteItems.push(prediction);
                    });
                    this.cd.detectChanges();
                });
            });
    }

    focusOut() {
        this.inputFocused = false;
        this.myDescription.setFocus();
    }

    selectSearchResult(item) {
        this.autocomplete.input = item.description;
        this.selectedPlaceId = item.place_id;
        this.focusOut();
    }

    back() {
        this.navController.pop();
    }

    async showDeletePhotoAlert(photoIndex: number) {
        const alert = await this.alertController.create({
            header: "Supprimer la photo",
            message: "Es-tu sûr de vouloir supprimer la photo?",
            buttons: [
                {
                    text: "Annuler",
                    role: 'cancel',
                    handler: (blah) => {
                        this.myPhotos[photoIndex].clicked = false;
                    }
                },
                {
                    text: "Confirmer",
                    handler: () => {
                        this.myPhotos.splice(photoIndex, 1);
                    }
                }
            ]
        });

        await alert.present();
    }

    detectChanges() {
        if (this.cd && !(this.cd as ViewRef).destroyed) {
            this.cd.detectChanges();
        }
    }

    async openPhotoChoice() {
        if (this.platform.is('capacitor')) {
            const actionSheet = await this.actionSheetController.create({
                cssClass: 'action-sheets-basic-page',
                buttons: [
                    {
                        text: 'Prendre une photo',
                        role: 'destructive',
                        icon: 'camera-outline',
                        handler: () => this.getImageFromSource(CameraSource.Camera)
                    },
                    {
                        text: 'Choisir dans la galerie',
                        icon: 'images-outline',
                        handler: () => this.getImageFromSource(CameraSource.Photos)
                    },
                ]
            });
            await actionSheet.present();
        } else {
            this.getImageFromSource(CameraSource.Photos);
        }
    }

    private getImageFromSource(source: CameraSource): void {
        Camera.getPhoto({
            resultType: CameraResultType.Uri,
            source,
        }).then((pic) => {
            this.myPhotos.push({
                img: 'loading',
                clicked: false
            });
            this.myPhotos[this.myPhotos.length - 1] = {
                img: pic.webPath,
                clicked: false
            };
        });
    }

    buttonDisabled() {
        return this.nom == '' || !this.selectedPlaceId || this.myPhotos.length == 0;
    }

    valider() {
        this.userService.presentLoading("Création de la cuisine").then(() => {
            const browserMap = new google.maps.Map(document.createElement('map_canvas'), null);
            this.GooglePlaces = new google.maps.places.PlacesService(browserMap);
            const request = {
                placeId: this.selectedPlaceId,
                fields: ['geometry']
            };
            this.GooglePlaces.getDetails(request, (results, status) => {
                let locationData = new firebase.firestore.GeoPoint(Number(results.geometry.location.lat()), Number(results.geometry.location.lng()));

                //enregistrer demande dans bdd
                let newCuisine = {
                    adresse: this.autocomplete.input,
                    avis: [],
                    description: this.description,
                    geoloc: locationData,
                    menu: {
                        boissons: [],
                        desserts: [],
                        entrees: [],
                        plats: []
                    },
                    nbBoxPreparees: 0,
                    nom: this.nom,
                    photos: [],
                    token: this.userService.token
                };
                this.myPhotos.forEach((photo, index) => {
                    this.uploadImage(this.userService.userInfo.uid, newCuisine, this.userService.userInfo.uid + '-' + (index + 1), photo.img);
                });
            });
        });
    }

    photosDownloadUrls = [];

    uploadImage(spotId, cuisine, imageName, imageURI) {
        return new Promise<any>((resolve, reject) => {
            let storageRef = firebase.storage().ref();
            let imageRef = storageRef.child('Cuisines').child(imageName + '.jpg');
            let self = this;
            this.encodeImageUri(this.platform.is('capacitor') ? imageURI : this.domSanitizer.sanitize(SecurityContext.RESOURCE_URL,imageURI), function (image64) {
                imageRef.putString(image64, 'data_url')
                    .then(snapshot => {
                        imageRef.getDownloadURL().then((lienImg) => {
                            self.photosDownloadUrls.push(lienImg);
                            if (self.photosDownloadUrls.length == self.myPhotos.length) {
                                cuisine.photos = self.photosDownloadUrls;
                                const db = firebase.firestore();
                                db.collection('cuisines').doc(self.userService.userInfo.uid).set(cuisine).then(function () {
                                    let navigationExtras: NavigationExtras = {
                                        replaceUrl: true,
                                        queryParams: {
                                            id: self.userService.userInfo.uid
                                        }
                                    };
                                    self.router.navigate(['stripe-account'], navigationExtras).then(() => {
                                        self.userService.loadingController.dismiss();
                                    });
                                });
                            }
                        });
                    }, err => {
                        reject(err);
                    });
            });
        });
    }

    encodeImageUri(imageUri, callback) {
        var c = document.createElement('canvas');
        var ctx = c.getContext('2d');
        var img = new Image();
        img.onload = function () {
            var aux: any = this;
            c.width = aux.width;
            c.height = aux.height;
            ctx.drawImage(img, 0, 0);
            var dataURL = c.toDataURL('image/jpeg');
            callback(dataURL);
        };
        img.src = imageUri;
    };

}
