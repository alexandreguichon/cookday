import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full'
  },
  {
    path: 'login',
    loadChildren: () => import('./pages/login/login.module').then( m => m.LoginPageModule)
  },
  {
    path: 'register',
    loadChildren: () => import('./pages/register/register.module').then( m => m.RegisterPageModule)
  },
  {
    path: 'confirm-phone',
    loadChildren: () => import('./pages/confirm-phone/confirm-phone.module').then( m => m.ConfirmPhonePageModule)
  },
  {
    path: 'terms',
    loadChildren: () => import('./pages/terms/terms.module').then( m => m.TermsPageModule)
  },
  {
    path: 'principale',
    loadChildren: () => import('./pages/principale/principale.module').then( m => m.PrincipalePageModule)
  },
  {
    path: 'cuisine',
    loadChildren: () => import('./pages/cuisine/cuisine.module').then( m => m.CuisinePageModule)
  },
  {
    path: 'vente',
    loadChildren: () => import('./pages/vente/vente.module').then( m => m.VentePageModule)
  },
  {
    path: 'commande',
    loadChildren: () => import('./pages/commande/commande.module').then( m => m.CommandePageModule)
  },
  {
    path: 'chat',
    loadChildren: () => import('./pages/chat/chat.module').then( m => m.ChatPageModule)
  },
  {
    path: 'noter-commande',
    loadChildren: () => import('./pages/noter-commande/noter-commande.module').then( m => m.NoterCommandePageModule)
  },
  {
    path: 'signaler',
    loadChildren: () => import('./pages/signaler/signaler.module').then( m => m.SignalerPageModule)
  },
  {
    path: 'a-propos',
    loadChildren: () => import('./pages/a-propos/a-propos.module').then( m => m.AProposPageModule)
  },
  {
    path: 'devenir-cuisine',
    loadChildren: () => import('./pages/devenir-cuisine/devenir-cuisine.module').then( m => m.DevenirCuisinePageModule)
  },
  {
    path: 'ajouter-plat-cuisine',
    loadChildren: () => import('./pages/ajouter-plat-cuisine/ajouter-plat-cuisine.module').then( m => m.AjouterPlatCuisinePageModule)
  },
  {
    path: 'modifier-cuisine',
    loadChildren: () => import('./pages/modifier-cuisine/modifier-cuisine.module').then( m => m.ModifierCuisinePageModule)
  },
  {
    path: 'nouvelle-vente',
    loadChildren: () => import('./pages/nouvelle-vente/nouvelle-vente.module').then( m => m.NouvelleVentePageModule)
  },
  {
    path: 'finaliser-commande',
    loadChildren: () => import('./pages/finaliser-commande/finaliser-commande.module').then( m => m.FinaliserCommandePageModule)
  },
  {
    path: 'modifier-telephone',
    loadChildren: () => import('./pages/modifier-telephone/modifier-telephone.module').then( m => m.ModifierTelephonePageModule)
  },
  {
    path: 'photo-details',
    loadChildren: () => import('./pages/photo-details/photo-details.module').then( m => m.PhotoDetailsPageModule)
  },
  {
    path: 'evaluations-cuisine',
    loadChildren: () => import('./pages/evaluations-cuisine/evaluations-cuisine.module').then( m => m.EvaluationsCuisinePageModule)
  },
  {
    path: 'map',
    loadChildren: () => import('./pages/map/map.module').then( m => m.MapPageModule)
  },
  {
    path: 'stripe-account',
    loadChildren: () => import('./pages/stripe-account/stripe-account.module').then( m => m.StripeAccountPageModule)
  },
  {
    path: 'stripe-account-redirect',
    loadChildren: () => import('./pages/stripe-account-redirect/stripe-account-redirect.module').then( m => m.StripeAccountRedirectPageModule)
  },
  {
    path: 'stripe-ok/:id',
    loadChildren: () => import('./pages/stripe-ok/stripe-ok.module').then( m => m.StripeOkPageModule)
  },
  {
    path: 'stripe-nope',
    loadChildren: () => import('./pages/stripe-nope/stripe-nope.module').then( m => m.StripeNopePageModule)
  },  {
    path: 'change-mdp',
    loadChildren: () => import('./pages/change-mdp/change-mdp.module').then( m => m.ChangeMdpPageModule)
  },



];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
