import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Injectable} from "@angular/core";
import {Observable, throwError} from "rxjs";
import {catchError, take} from "rxjs/operators";

import {
    CountryListInterface,
    Country,
} from "../interfaces/interface";

const apiUrl = "https://restcountries.eu/rest/v2";

@Injectable({
    providedIn: "root",
})

export class CountriesService {

    constructor(private httpClient: HttpClient) {
    }

    countries: any;
    searchItems: Array<Country> = [];
    loadingInfo = false;
    countryChosen = false;
    public country: any;
    public countryName = "";
    selectedCountry = {
        name: "France",
        flag: "https://cdn.countryflags.com/thumbs/france/flag-3d-round-250.png",
        dial_code: ["+33"]
    }

    jsonUrl = 'assets/countries.json';

    public list(): Observable<any[]> {
        return this.httpClient.get<any[]>(this.jsonUrl);
    }

    async getCountryList() {
        return this.fetchCountryListData("").subscribe(
            (data: any[]) => {
                this.countries = data;
                return true;
            },
            (error) => {
                console.log("error fetching country list info: ", error);
                return true;
            }
        );
    }

    fetchCountryListData(url: string) {
        return this.httpClient.get<CountryListInterface[]>(this.jsonUrl).pipe(
            take(1),
            catchError((error) => {
                return throwError("Countries not found, error:", error);
            })
        );
    }
}
