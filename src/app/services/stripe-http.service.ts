import {Injectable, NgZone} from "@angular/core";
import {HttpClient, HttpHeaders, HttpParams} from "@angular/common/http";
import {Observable} from "rxjs";
import {PaymentIntent} from "@stripe/stripe-js";
import {environment} from "../../environments/environment";
import {map} from "rxjs/operators";
import {UserService} from "./user.service";

@Injectable({
    providedIn: 'root'
})
export class StripeHttpService {

    private readonly stripeBaseUrl = 'https://api.stripe.com';

    private readonly stripeFees: number = 57;

    accountActivated: boolean = false;

    private readonly authHeader = new HttpHeaders()
        .set('Authorization', 'Bearer ' + environment.stripeSecret)
        .set("Content-Type", "application/x-www-form-urlencoded");


    constructor(private readonly httpClient: HttpClient,
                private readonly userService: UserService,
                private ngZone: NgZone) {
    }

    public createPaymentIntentWithTransfer(amount: number, accountId: string): Observable<PaymentIntent> {
        const app_fee = Math.round(0.2 * amount);
        const intent = new HttpParams()
            .set('amount', amount + this.stripeFees)
            .set('currency', 'eur')
            .set('application_fee_amount', app_fee)
            .set('transfer_data[destination]', accountId);
        return this.httpClient.post<PaymentIntent>(
            this.stripeBaseUrl + '/v1/payment_intents',
            intent.toString(),
            {headers: this.authHeader});
    }

    public cancelPaymentIntent(clientSecret: string): Observable<PaymentIntent> {
        const cancellationReason = new HttpParams()
            .set('cancellation_reason', 'abandoned');
        return this.httpClient.post<PaymentIntent>(
            this.stripeBaseUrl + '/v1/payment_intents/' + clientSecret + '/cancel',
            cancellationReason.toString(),
            {headers: this.authHeader});
    }

    public createAccount(): Observable<any> {
        const account = new HttpParams()
            .set('type', 'standard');
        return this.httpClient.post<any>(this.stripeBaseUrl + '/v1/accounts', account.toString(), {headers: this.authHeader});
    }

    public isActiveAccount(accountId: string): Observable<boolean> {
        return this.httpClient.get<any>(this.stripeBaseUrl + '/v1/accounts/' + accountId, {headers: this.authHeader}).pipe(
            map((account) => account.details_submitted && account.charges_enabled)
        );
    }

    public createAccountLink(accountId: string, id: string): Observable<any> {
        const accountLink = new HttpParams()
            .set('account', accountId)
            .set('refresh_url', 'https://cookday.fr/stripe-nope')
            .set('return_url', 'https://cookday.fr/stripe-ok/'+ id)
            .set('type', 'account_onboarding');
        return this.httpClient.post<any>(this.stripeBaseUrl + '/v1/account_links', accountLink.toString(), {headers: this.authHeader});
    }

}
