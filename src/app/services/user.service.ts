import {Injectable} from '@angular/core';
import {
    AlertController,
    LoadingController,
    ModalController,
    Platform,
    PopoverController,
    ToastController
} from "@ionic/angular";
import {Geolocation} from '@ionic-native/geolocation/ngx';
import {AndroidPermissions} from "@ionic-native/android-permissions/ngx";
import {LocationAccuracy} from '@ionic-native/location-accuracy/ngx';
import firebase from 'firebase/app';
import {PanierComponent} from "../components/panier/panier.component";
import {LaunchNavigator} from '@ionic-native/launch-navigator/ngx';
import {Router} from "@angular/router";
import {AngularFireAuth} from "@angular/fire/auth";
import {PushNotifications} from "@capacitor/push-notifications";
import {NotifyComponent} from "../components/notify/notify.component";
import {from, merge} from "rxjs";
import {share, switchMap, tap} from "rxjs/operators";
import {GoogleMap} from "@capacitor-community/google-maps";

@Injectable({
    providedIn: 'root'
})
export class UserService {

    map: string;

    recaptchaVerifier: firebase.auth.RecaptchaVerifier;
    confirmationResult: firebase.auth.ConfirmationResult;

    db = firebase.firestore();
    selectedLanguage = "fr";
    userlat: number;
    userlng: number;

    userInfo: any = {};
    //uid: string = "XPDthZ8bt4gmKrmgBRw7";
    user: any = null;

    commandesData: any = {};
    commandesRecues: any = {};
    token: string = null;

    users = {};

    constructor(
        public toastCtrl: ToastController,
        public platform: Platform,
        private geolocation: Geolocation,
        private androidPermissions: AndroidPermissions,
        private locationAccuracy: LocationAccuracy,
        public popoverController: PopoverController,
        private launchNavigator: LaunchNavigator,
        private router: Router,
        private alertController: AlertController,
        private angularFireAuth: AngularFireAuth,
        public loadingController: LoadingController,
        public modalController: ModalController,
        private readonly toastController: ToastController,
    ) {

        PushNotifications.requestPermissions();
        const notifRegister$ = from(PushNotifications.requestPermissions()).pipe(
            switchMap(() => PushNotifications.register()),
            share()
        );

        const foreground$ = notifRegister$.pipe(
            switchMap(() => PushNotifications.addListener('pushNotificationReceived', notification => {
                toastController.create({
                    position: 'top',
                    header: notification.title,
                    message: notification.body,
                    duration: 5000
                })
                    .then((toast) => toast.present());
            }))
        );

        merge(notifRegister$,foreground$).subscribe();
    }

    async getData() {
        let self = this;
        await this.db.collection('utilisateurs').doc(this.userInfo.uid).onSnapshot(user => {
            if (!self.user) {
                self.user = user.data();
                self.registerPush();
                if (!self.user.token && this.token) {
                    this.user.token = this.token;
                    this.db.collection("utilisateurs").doc(this.userInfo.uid).update({
                        token: this.token
                    });
                    this.db.collection("cuisines").doc(this.userInfo.uid).update({
                        token: this.token
                    })
                }
                self.user.id = user.id;
                this.db.collection('commandes').where("user", "==", user.id).get()
                    .then((querySnapshot) => {
                        querySnapshot.forEach((doc) => {
                            // doc.data() is never undefined for query doc snapshots
                            let newDoc = doc.data();
                            newDoc.dateRecup = doc.data().dateRecup.toDate();
                            newDoc.dateRes = doc.data().dateRes.toDate();
                            self.commandesData[doc.id] = newDoc;
                        });
                        return self.router.navigate(['principale']).then(() => {
                            self.loadingController.dismiss();
                        });
                    });
                this.db.collection('commandes').where("cuisine", "==", user.id).onSnapshot((querySnapshot) => {
                    var tempCommandes = {};
                    querySnapshot.forEach((doc) => {
                        let newDoc = doc.data();
                        newDoc.dateRecup = doc.data().dateRecup.toDate();
                        newDoc.dateRes = doc.data().dateRes.toDate();
                        this.db.collection('utilisateurs').doc(newDoc.user).get().then(user2 => {
                            newDoc['userData'] = user2.data();
                        }).then(() => {
                            tempCommandes[doc.id] = newDoc;
                        })
                    });
                    self.commandesRecues = tempCommandes;
                });
            } else {
                self.user = user.data();
            }
        });
    }

    async toast(message: string, duration: number) {
        const toast = await this.toastCtrl.create({
            message: message,
            duration: duration
        });
        toast.present();
    }

    async getMyLocation() {
            this.getLocationCoordinates();
    }

    // requestGPSPermission() {
    //     this.locationAccuracy.canRequest().then((canRequest: boolean) => {
    //         if (canRequest) {
    //
    //         } else {
    //             //Show 'GPS Permission Request' dialogue
    //             this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.ACCESS_COARSE_LOCATION)
    //                 .then(
    //                     () => {
    //                         // call method to turn on GPS
    //                         //this.askToTurnOnGPS();
    //                     },
    //                     error => {
    //                         //Show alert if user click on 'No Thanks'
    //                         alert("BigBalls se sert de ta position pour te proposer les terrains les plus proches et t'indiquer ta distance par rapport à eux. Si tu souhaites bénéficier de ces fonctionnalités, active ta géolocalisation.");
    //                     }
    //                 );
    //         }
    //     });
    // }
    //
    // askToTurnOnGPS() {
    //     this.locationAccuracy.request(this.locationAccuracy.REQUEST_PRIORITY_HIGH_ACCURACY).then(
    //         () => {
    //             // When GPS Turned ON call method to get Accurate location coordinates
    //             this.getLocationCoordinates();
    //         },
    //         error => alert('Error requesting location permissions ' + JSON.stringify(error))
    //     );
    // }

    getLocationCoordinates() {
        if (this.platform.is('capacitor')) {
            this.geolocation.getCurrentPosition().then((resp) => {
                this.setLat(resp.coords.latitude);
                this.setLng(resp.coords.longitude);
            }).catch((error) => {
                this.setLat(48.6851727);
                this.setLng(6.1797314);
                //alert('Error getting location' + JSON.stringify(error));
            });
        } else {
            if (navigator.geolocation) {
                let self = this;
                navigator.geolocation.getCurrentPosition(function (position) {
                    self.setLat(position.coords.latitude);
                    self.setLng(position.coords.longitude);
                });
            } else {
                console.log('Geolocation is not supported by this browser.');
            }
        }
    }

    setLat(lat: number) {
        this.userlat = lat;
    }

    setLng(lng: number) {
        this.userlng = lng;
    }

    nbNotifsNonLues() {
        if (this.user) {
            return this.getNotifs().filter((e: any) => !e.lu).length;
        }
    }

    setAllNotifsLues() {
        this.user.notifs.forEach(notif => {
            this.setNotifLue(notif);
        });
        this.toast("Toutes les notifications ont été mises en lues", 2000);
    }

    setNotifLue(notif) {
        notif.lu = true;
        this.save();
    }

    async save() {
        return this.db.collection('utilisateurs').doc(this.user.id).update(this.user);
    }

    getNotifs() {
        return Object.values(this.user.notifs.reduce((acc, post) => {
            let key = (post.cuisine==this.userInfo.uid) ? 'user' : 'cuisine'
            if (!acc[post[key]] || post.date > acc[post[key]].date) {
                acc[post[key]] = post;
            }
            return acc;
        }, {})).sort((a: any, b: any) => b.date - a.date);
    }

    async presentPanierPopover() {
        const popover = await this.popoverController.create({
            component: PanierComponent,
            translucent: true,
            animated: false,
            cssClass: 'panier-popover'
        });
        return await popover.present();
    }

    openMap(cuisine: any) {
        if(this.platform.is('capacitor')){
            this.launchNavigator.navigate([cuisine.geoloc.latitude, cuisine.geoloc.longitude], {
                destinationName: cuisine.nom,
                startName: 'Vous'
            })
                .then(
                    success => console.log('Launched navigator'),
                    error => console.log('Error launching navigator', error)
                ).catch(err => {
                console.log('Error Launched navigator ' + err);
            });
        }
        else{
            window.open("https://www.google.com/maps/place/?q=" + cuisine.geoloc.latitude + " " + cuisine.geoloc.longitude);
        }
    }

    getCommandes(cuisine) {
        let res: any[] = Object.values(this.user.commandes);
        return res.filter(c => c.cuisine == cuisine.id).sort((a: any, b: any) => {
            if (b.statut == 'à récupérer') {
                return 1;
            }

            if (a.statut == 'à récupérer') {
                return -1;
            }

            return 0;
        });
    }

    aRecuperer() {
        let res = Object.values(this.user.commandes) as any[];
        return res.filter(c => c.statut == "à récupérer").sort((a: any, b: any) => a.date - b.date);
    }

    phone() {
        return this.user.phone.callingCode + " " + this.user.phone.num;
    }

    async presentAlertDeconnexion() {
        const alert = await this.alertController.create({
            header: "Confirmer la déconnexion",
            message: "Vous êtes sur le point de vous déconnecter. Confirmez-vous la déconnexion?",
            buttons: [
                {
                    text: 'Annuler',
                    role: 'cancel',
                    cssClass: 'secondary',
                    handler: (blah) => {
                    }
                },
                {
                    text: 'Me déconnecter',
                    handler: () => {
                        this.logout();
                    }
                }
            ]
        });

        await alert.present();
    }

    async presentAlertSuppression() {
        const alert = await this.alertController.create({
            header: "Confirmer la suppression du compte",
            message: "Vous êtes sur le point de supprimer votre compte. Toutes vos données seront supprimées et vous ne pourrez plus accéder à CookDay. Confirmez-vous cette action?",
            buttons: [
                {
                    text: 'Annuler',
                    role: 'cancel',
                    cssClass: 'secondary',
                    handler: (blah) => {
                    }
                },
                {
                    text: 'Supprimer mon compte',
                    handler: () => {
                        this.deleteAccount();
                    }
                }
            ]
        });

        await alert.present();
    }

    deleteAccount() {
        this.presentLoading("Suppression des données en cours").then(() => {
            Promise.all([
                this.router.navigate(['login']),
                this.db.collection('utilisateurs').doc(this.userInfo.uid).delete(),
                firebase.auth().currentUser.delete()
            ]).then(() => {
                this.loadingController.dismiss();
                this.toast("Le compte a été supprimé", 2000);
            })
        })
    }

    logout() {
        this.angularFireAuth.signOut();
        this.router.navigate(['login']).then(() => {
            this.userInfo = null;
            this.user = null;
            this.toast("Vous avez été déconnecté.", 2000);
        });
    }

    SendVerificationMail() {
        return this.angularFireAuth.currentUser.then(user => {
            user.sendEmailVerification().then(() => {
                this.router.navigate(['connexion']).then(() => {
                    this.alertVerifEmail(user.email);
                })
            });
        });
    }

    async alertVerifEmail(email: string) {
        const alert = await this.alertController.create({
            header: "Email envoyé",
            message: "Un email de vérification a été envoyé à " + email,
            buttons: [
                {
                    text: "Compris",
                    role: 'cancel',
                    cssClass: 'primary',
                    handler: (blah) => {
                    }
                },
            ]
        });
        await alert.present();
    }

    async presentLoading(message) {
        const loading = await this.loadingController.create({
            message: message
        });
        await loading.present();
    }

    updatePanier() {
        const db = firebase.firestore();
        db.collection('utilisateurs').doc(this.userInfo.uid).update({
            panier: this.user.panier
        });
    }

    async setCommandeStatut(commande: any, statut: string) {
        commande.statut = statut;
        this.toast('La commande est ' + statut + '.', 1000);
        return this.db.collection('commandes').doc(commande.id).update({
            statut: statut
        });
    }

    isIos() {
        return this.platform.is('ios');
    }

    getMargin() {
        return this.isIos() ? "45px" : "10px";
    }

    getImgHeight() {
        return (!this.platform.is('tablet') && this.platform.is('capacitor')) ? '150px' : '300px';
    }

    getUserProfileImg(id: string) {
        firebase.storage().ref(id + '/photoProfil.jpg').getDownloadURL().then(url => {
            return url;
        });
    }

    getUserById(id: string){
        if(this.users[id]){
            return this.users[id];
        }
        else{
            this.users[id] = {
                prenom: '...',
                photo: ''
            }
            this.db.collection('utilisateurs').doc(id).onSnapshot(user => {
               this.users[id] = user.data();
            });
            return this.users[id];
        }
    }

    initPush() {
        if (this.platform.is('capacitor')) {
            this.registerPush();
        }
    }

    async registerPush() {
        PushNotifications.requestPermissions().then((permission) => {
            if (permission.receive == "granted") {
                // Register with Apple / Google to receive push via APNS/FCM
                this.registerToken();
                PushNotifications.register();
            } else {
                // No permission for push granted
            }
        });

await PushNotifications.addListener('registration', token => {
    console.info('Registration token: ', token.value);
  });

        await PushNotifications.addListener('registrationError', err => {
            console.error('Registration error: ', err.error);
        });

        await PushNotifications.addListener('pushNotificationReceived', notification => {
            console.log('Push notification received: ', JSON.stringify(notification));
            if(notification.data && notification.data.action){
                this.presentNotify(notification.data);
            }
        });

        await PushNotifications.addListener('pushNotificationActionPerformed', notification => {
            console.log('Push notification action performed', notification.actionId, notification.inputValue);
        });

        let permStatus = await PushNotifications.checkPermissions();

        if (permStatus.receive === 'prompt') {
            permStatus = await PushNotifications.requestPermissions();
        }

        if (permStatus.receive !== 'granted') {
            throw new Error('User denied permissions!');
        }

        const notificationList = await PushNotifications.getDeliveredNotifications();
        console.log('delivered notifications', notificationList);
    }

    async registerToken() {
        await PushNotifications.addListener('registration', token => {
            this.token = token.value;
            if (this.user && (this.user.token!=token.value)) {
                this.user.token = this.token;
                this.db.collection("utilisateurs").doc(this.userInfo.uid).update({
                    token: this.token
                });
                this.db.collection("cuisines").doc(this.userInfo.uid).update({
                    token: this.token
                })
            }
        });
        PushNotifications.register();
    }

    async presentNotify(notif: any) {
        const modal = await this.modalController.create({
            component: NotifyComponent,
            componentProps: {notif: notif},
            showBackdrop: false,
            cssClass: 'my-notify-css'
        });
        // this.vibration.vibrate(50);
        return await modal.present();
    }
}
