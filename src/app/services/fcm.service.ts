import {Injectable} from '@angular/core';
import {PushNotifications} from '@capacitor/push-notifications';
import {Router} from '@angular/router';
import {ModalController, Platform} from "@ionic/angular";
import {HttpClient} from '@angular/common/http';
import {UserService} from "./user.service";
import {VentesService} from "./ventes.service";
import {NotifyComponent} from "../components/notify/notify.component";
import firebase from 'firebase/app';
import { map } from "rxjs/operators";

@Injectable({
    providedIn: 'root'
})
export class FcmService {

    constructor(private router: Router,
                private platform: Platform,
                private http: HttpClient,
                private ventesService: VentesService,
                private userService: UserService,
    ) {

    }

    sendChatNotify(text: string, objectId: string, cuisine: string, user: string) {
        let notif = {
            cuisine: cuisine,
            user: user,
            date: new Date(),
            text: text,
            action: "message",
            objectId: objectId
        };
        this.userService.db.collection('utilisateurs').doc(cuisine == this.userService.userInfo.uid ? user : cuisine).update({
            notifs: firebase.firestore.FieldValue.arrayUnion(notif)
        }).then(res => {
            this.sendPostRequest(
                cuisine == this.userService.userInfo.uid ? this.ventesService.getCuisineById(cuisine).nom : this.userService.getUserById(user).nom,
                text,
                cuisine == this.userService.userInfo.uid ? this.ventesService.getCuisineById(cuisine).photos[0] : this.userService.getUserById(user).photo,
                notif,
                "",
                this.userService.userInfo.uid,
                cuisine == this.userService.userInfo.uid ? this.userService.getUserById(user).token : this.ventesService.getCuisineById(cuisine).token,
                "",
                "message",
                "")
        })
    }

    sendCommandeNotify(commande: any, statut: string) {
        let notif = {
            commande: commande,
            statut: statut,
            date: new Date(),
            text: "Votre commande est passé au statut '" + statut + "'",
            action: "commande",
            objectId: commande.id
        };
        this.userService.db.collection('utilisateurs').doc(this.userService.getUserById(commande.user)).update({
            notifs: firebase.firestore.FieldValue.arrayUnion(notif)
        }).then(res => {
            this.sendPostRequest(
                statut[0].toUpperCase() + statut.substr(1).toLowerCase(),
                "Votre commande est passé au statut '" + statut + "'",
                this.ventesService.getCuisineById(commande.cuisine).photos[0],
                notif,
                "",
                this.userService.userInfo.uid,
                this.userService.getUserById(commande.user),
                "",
                "commande",
                commande.id)
        })
    }

    sendPostRequest(title: string, body: string, image: string, data: any, destination: string, by: string, to: string, message: any, action: string, objectId: string) {
        var key = 'AAAA0ff3LDk:APA91bGOY6WbAkWOkzxilzZuUIUCY3tkzGqQWH6wEPFAeTT0kKb1q9TGWPTajWT-ElCSFPZQ798YH2mdXtOsPc4n-66O1EC9dN83su24I48UoV_nLYl-2nDEPE8n3Ccs0XbUM8F4B6u5';
        var notification = {
            'title': title,
            'body': body,
            'icon': 'assets/icon/favicon.png',
            'click_action': 'FCM_PLUGIN_ACTIVITY',
        };

        fetch('https://fcm.googleapis.com/fcm/send', {
            'method': 'POST',
            'headers': {
                'Authorization': 'key=' + key,
                'Content-Type': 'application/json'
            },
            'body': JSON.stringify({
                'notification': notification,
                'to': to
            })
        }).then(function(response) {
            console.log(response);
        }).catch(function(error) {
            console.error(error);
        })
    }

    // sendPostRequest(title: string, body: string, image: string, data: any, destination: string, by: string, to: string, message: any, action: string, objectId: string) {
    //
    //     // if (to && to != this.userService.userInfo.uid) {
    //     if (to) {
    //         let headers = {
    //             'Content-Type': 'application/json',
    //             'Accept': 'application/json',
    //             'Authorization': 'key=AAAA0ff3LDk:APA91bGOY6WbAkWOkzxilzZuUIUCY3tkzGqQWH6wEPFAeTT0kKb1q9TGWPTajWT-ElCSFPZQ798YH2mdXtOsPc4n-66O1EC9dN83su24I48UoV_nLYl-2nDEPE8n3Ccs0XbUM8F4B6u5',
    //         };
    //
    //         this.http.setServerTrustMode('nocheck');
    //         this.http.setHeader('*', 'Access-Control-Allow-Origin', '*');
    //         this.http.setHeader('*', 'Access-Control-Allow-Methods', 'POST, GET, OPTIONS, PUT');
    //         this.http.setHeader('*', 'Accept', 'application/json');
    //         this.http.setHeader('*', 'content-type', 'application/json');
    //         this.http.setHeader('*', 'Authorization', 'key=AAAA0ff3LDk:APA91bGOY6WbAkWOkzxilzZuUIUCY3tkzGqQWH6wEPFAeTT0kKb1q9TGWPTajWT-ElCSFPZQ798YH2mdXtOsPc4n-66O1EC9dN83su24I48UoV_nLYl-2nDEPE8n3Ccs0XbUM8F4B6u5');
    //         //Important to set the data serializer or the request gets rejected
    //         this.http.setDataSerializer('json');
    //
    //         /*let options = {
    //             headers: headers
    //         };*/
    //
    //         let postData = {
    //             'notification': {
    //                 'title': title,
    //                 'body': body,
    //                 'image': 'assets/icon/favicon.png',
    //                 'sound': 'default',
    //                 'click_action': 'FCM_PLUGIN_ACTIVITY',
    //                 'icon': 'fcm_push_icon',
    //             },
    //             'data': data,
    //             //Pour envoyer à tous les utilisateurs, to='all'
    //             'to': to,
    //             'priority': 'high',
    //             'restricted_package_name': ''
    //         };
    //         let method = "post" as const;
    //
    //         const options = {
    //             method: method,
    //             data: postData,
    //             headers: headers,
    //         };
    //
    //         this.http.sendRequest('https://fcm.googleapis.com/fcm/send', options)
    //             .then(data => {
    //                 console.log(JSON.stringify(data))
    //             }, error => {
    //                 console.log('Error :' + error);
    //                 console.log(JSON.stringify(error));
    //             });
    //     }
    //
    // }
}
