// @ts-ignore
import {Injectable} from '@angular/core';
import {UserService} from "./user.service";
import firebase from 'firebase/app';
import {LoadingController, PopoverController, ToastController} from "@ionic/angular";
import {NavigationExtras, Router} from "@angular/router";
import {PlatComponent} from "../components/plat/plat.component";

// @ts-ignore
@Injectable({
    providedIn: 'root'
})
export class VentesService {

    constructor(
        public userService: UserService,
        public toastCtrl: ToastController,
        private router: Router,
        public popoverController: PopoverController,
        public loadingController: LoadingController
    ) {
    }

    ventes = {};
    cuisines = {};
    commandes = {};

    newVente = {
        date: new Date(),
        heureDebut: new Date(),
        heureFin: new Date(),
        menu: {
            entrees: [],
            plats: [],
            desserts: [],
            boissons: []
        }
    }

    /*panier = {
        cuisine: "ecLniR6K7fdL9r6uskBw",
        vente: "wZMF4vF7O30gbIn5UK1g",
        menu: {
            entrees: [],
            plats: [
                {
                    nom: "Tagliatelles au saumon",
                    prix: 7,
                    quantite: 1,
                    remarque: ""
                }
            ],
            desserts: [
                {
                    nom: "Tiramisu",
                    prix: 4,
                    quantite: 2,
                    remarque: "Sans amandes"
                }
            ],
            boissons: [],
        },
        ajoutCouvertsServiettes: true
    }*/

    order = ['entrées', 'plats', 'desserts', 'boissons'];


    ventesArray() {
        return Object.values(this.ventes);
    }

    async getData() {
        const db = firebase.firestore();
        let self = this;
        let currentDate = new Date();
        let tempVentes = {};
        let tempCuisines = {};
        let tempCommandes = {};
        Promise.all([
            db.collection('ventes')
                //.where("date", ">=", currentDate.toISOString())
                .onSnapshot(function (querySnapshot) {
                    querySnapshot.forEach(vente => {
                        let newDoc = vente.data();
                        newDoc.id = vente.id;
                        newDoc.date = newDoc.date.toDate();
                        newDoc.horaires.debut = newDoc.horaires.debut.toDate();
                        newDoc.horaires.fin = newDoc.horaires.fin.toDate();
                        self.order.forEach(cat => {
                            newDoc.menu[self.retirerAccents(cat)].forEach(plat => {
                                plat.quantiteAAjouter = 0;
                            })
                        })
                        tempVentes[vente.id] = newDoc;
                    });
                    return true;
                }),
            db.collection('cuisines')
                .onSnapshot(function (querySnapshot) {
                    querySnapshot.forEach(cuisine => {
                        let newDoc = cuisine.data();
                        newDoc.id = cuisine.id;
                        tempCuisines[cuisine.id] = newDoc;
                        if(cuisine.id==self.userService.userInfo.uid && self.userService.token){
                            db.collection("cuisines").doc(self.userService.userInfo.uid).update({
                                token: self.userService.token
                            })
                        }
                    });
                    return true;
                }),
            db.collection('ventes')
                //.where("date", ">=", currentDate.toISOString())
                .onSnapshot(function (querySnapshot) {
                    querySnapshot.forEach(vente => {
                        let newDoc = vente.data();
                        newDoc.id = vente.id;
                        newDoc.date = newDoc.date.toDate();
                        newDoc.horaires.debut = newDoc.horaires.debut.toDate();
                        newDoc.horaires.fin = newDoc.horaires.fin.toDate();
                        self.order.forEach(cat => {
                            newDoc.menu[self.retirerAccents(cat)].forEach(plat => {
                                plat.quantiteAAjouter = 0;
                            })
                        })
                        tempVentes[vente.id] = newDoc;
                    });
                    return true;
                }),
        ]).then(() => {
            self.cuisines = tempCuisines;
            self.ventes = tempVentes;
            return true;
        });
    }

    getVentes(chosenDate: any) {
        let res = this.ventesArray() as any[];
        res = res.filter(v => this.getCuisine(v) ? this.getCuisine(v).stripeAccountId : false);
        res = res.filter(v => this.isSameDay(chosenDate, v.date));
        //sort by note
        res.sort((a: any, b: any) => this.getMoyenne(this.getCuisine(b)) - this.getMoyenne(this.getCuisine(a)));
        //sort by horaires.debut
        res.sort((a: any, b: any) => new Date(a.horaires.debut).getTime() - new Date(b.horaires.debut).getTime());
        //sort by distance
        return res.sort((a: any, b: any) => this.calculateDistance(this.getCuisine(a).geoloc) - this.calculateDistance(this.getCuisine(b).geoloc));
    }

    getVentesCuisine(cuisine) {
        let res: any[] = Object.values(this.ventes);
        return res.filter(v => v.cuisine == cuisine.id);
    }

    getCuisine(vente: any) {
        return this.cuisines[vente.cuisine];
    }

    getCuisineById(id: string){
        if(this.cuisines[id]){
            return this.cuisines[id];
        }
        else{
            this.cuisines[id] = {
                nom: '...',
                photos: ['']
            }
            this.userService.db.collection('cuisines').doc(id).onSnapshot(cuisine => {
                this.cuisines[id] = cuisine.data();
            });
            return this.cuisines[id];
        }
    }

    calculateDistance(geoloc: any): any {
        let lat1 = this.userService.userlat;
        let lat2 = geoloc.latitude;
        let long1 = this.userService.userlng;
        let long2 = geoloc.longitude;
        if (lat1 && lat2 && long1 && long2) {
            let p = 0.017453292519943295;    // Math.PI / 180
            let c = Math.cos;
            let a = 0.5 - c((lat1 - lat2) * p) / 2 + c(lat2 * p) * c((lat1) * p) * (1 - c(((long1 - long2) * p))) / 2;
            let dis = (12742 * Math.asin(Math.sqrt(a))); // 2 * R; R = 6371 km
            return dis.toFixed(1);
        } else {
            return 0;
        }
    }

    getMoyenne(cuisine) {
        if (cuisine && cuisine.avis.length) {
            let res = 0;
            cuisine.avis.forEach(avis => {
                res = res + avis.note;
            })
            return Math.round((res / cuisine.avis.length) * 10) / 10;
        } else {
            return 0;
        }
    }

    getPlats(obj) {
        let res = [];
        if (obj) {
            let self = this;
            this.order.forEach(function (e: any) {
                obj.menu[self.retirerAccents(e)].forEach(f => {
                    res.push(f);
                })
            })
        }
        return res;
    }

    async retirerPlat(plat) {
        let self = this;
        this.order.forEach(function (e: any) {
            self.userService.user.panier.menu[self.retirerAccents(e)] = self.userService.user.panier.menu[self.retirerAccents(e)].filter(p => p.nom != plat.nom);
        });
        if (this.getPanierLength() == 0) {
            this.popoverController.dismiss().then(() => {
                this.userService.user.panier = null;
                this.userService.updatePanier();
            })
        } else {
            this.userService.updatePanier();
        }
        return this.toast(plat.nom + " a été retiré du panier.", 2000);
    }

    searchVentes(recherche) {
        let res = this.ventesArray();
        res = res.filter(e =>
            this.filtrePlats(e, recherche) ||
            (this.getCuisine(e).nom.toLowerCase().indexOf(recherche.toLowerCase()) > -1));
        res.sort((a: any, b: any) => this.calculateDistance(this.getCuisine(a).geoloc) - this.calculateDistance(this.getCuisine(b).geoloc));
        res.sort((a: any, b: any) => new Date(a.horaires.debut).getTime() - new Date(b.horaires.debut).getTime());
        res.sort((a: any, b: any) => new Date(a.date).getTime() - new Date(b.date).getTime());
        return res as any[];
    }

    filtrePlats(vente, recherche) {
        return Object.values(vente.menu).some(function (e: any) {
            return e.some(f => {
                return f.nom.toLowerCase().indexOf(recherche.toLowerCase()) > -1;
            })
        })
    }

    prixFormat(val) {
        return (Math.round(val * 100) / 100).toFixed(2);
    }

    total(obj) {
        let res = 0;
        this.getPlats(obj).forEach(plat => {
            res += plat.prix * plat.quantite;
        })
        return res;
    }

    async toast(message: string, duration: number) {
        const toast = await this.toastCtrl.create({
            message: message,
            duration: duration
        });
        toast.present();
    }

    async open(path, id) {
        let navigationExtras: NavigationExtras = {
            queryParams: {
                id: JSON.stringify(id)
            }
        };
        return this.router.navigate([path], navigationExtras);
    }

    async openChat(cuisineId, userId) {
        let navigationExtras: NavigationExtras = {
            queryParams: {
                cuisineId: JSON.stringify(cuisineId),
                userId: JSON.stringify(userId),
            }
        };
        return this.router.navigate(['chat'], navigationExtras);
    }

    retirerAccents(str) {
        return str.normalize("NFD").replace(/[\u0300-\u036f]/g, "");
    }

    getIngredients(plat) {
        if (plat.ingredients.length == 0) {
            return null;
        } else {
            let res = plat.ingredients[0][0].toUpperCase() + plat.ingredients[0].substr(1).toLowerCase();
            for (let i = 1; i < plat.ingredients.length; i++) {
                res = res + ", " + plat.ingredients[i].toLowerCase()
            }
            return res;
        }
    }

    getPanierLength() {
        let res = 0;
        if (this.userService.user.panier && this.userService.user.panier.vente) {
            this.getPlats(this.userService.user.panier).forEach(e => {
                res += e.quantite
            })
        }
        return res;
    }

    async presentPlatPopover(plat) {
        const popover = await this.popoverController.create({
            component: PlatComponent,
            componentProps: {plat: plat},
            translucent: true,
            cssClass: 'plat-popover'
        });
        return await popover.present();
    }

    async noterCommande(id, note, remarque) {
        this.presentLoading("Votre note est en train d'être ajoutée.").then(() => {
            let newAvis = {
                commande: id,
                note: note,
                user: this.userService.userInfo.uid,
                remarque: remarque,
                date: new Date()
            };
            const db = firebase.firestore();
            return db.collection('cuisines').doc(this.userService.commandesData[id].cuisine).get().then(cuisine => {
                let avis = cuisine.data().avis;
                avis.push(newAvis);
                return db.collection('cuisines').doc(cuisine.id).update({
                    avis: avis
                }).then(() => {
                    this.cuisines[this.userService.commandesData[id].cuisine].avis.push(newAvis);
                    this.toast("Votre avis a été laissé pour " + this.cuisines[this.userService.commandesData[id].cuisine].nom, 2000);
                    this.router.navigate(['principale']);
                    this.loadingController.dismiss();
                })
            })
        })
    }

    async presentLoading(message) {
        const loading = await this.loadingController.create({
            message: message
        });
        await loading.present();
    }

    dejaNotee(cuisine, commande) {
        return cuisine.avis.some(avis => avis.commande == commande && avis.user == this.userService.userInfo.uid);
    }

    maCuisine() {
        return this.userService.userInfo.uid ? this.cuisines[this.userService.userInfo.uid] : null;
    }

    getQtePanier(plat) {
        let target = this.getPlats(this.userService.user.panier).find(e => e.nom == plat.nom)
        return target ? target.quantite : 0;
    }

    isSameDay(date1, date2) {
        return date1.getFullYear() == date2.getFullYear()
            && date1.getMonth() == date2.getMonth()
            && date1.getDate() == date2.getDate();
    }
}
