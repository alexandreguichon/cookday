import {LOCALE_ID, NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {RouteReuseStrategy} from '@angular/router';

import {IonicModule, IonicRouteStrategy} from '@ionic/angular';

import {AppComponent} from './app.component';
import {AppRoutingModule} from './app-routing.module';

import {FormsModule, ReactiveFormsModule} from '@angular/forms';

import {SelectCountryComponent} from "./components/select-country/select-country.component";
import {CountriesService} from "./services/countries.service";
import {HttpClientModule} from "@angular/common/http";
import { DatePipe } from '@angular/common';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import {AndroidPermissions} from "@ionic-native/android-permissions/ngx";
import {LocationAccuracy} from '@ionic-native/location-accuracy/ngx';
import { LaunchNavigator, LaunchNavigatorOptions } from '@ionic-native/launch-navigator/ngx';
import { registerLocaleData } from '@angular/common';
import localeFr from '@angular/common/locales/fr';
import { SMS } from '@ionic-native/sms/ngx';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { File } from '@ionic-native/file/ngx';
import {NativeGeocoder} from '@ionic-native/native-geocoder/ngx';

import {NotifCenterComponent} from "./components/notif-center/notif-center.component";
import {NotifItemComponent} from "./components/notif-item/notif-item.component";
import {PanierComponent} from "./components/panier/panier.component";
import {PlatComponent} from "./components/plat/plat.component";
import {SignalerComponent} from "./components/signaler/signaler.component";
import {AddElementVenteComponent} from "./components/add-element-vente/add-element-vente.component";
import {DetailsVentesComponent} from "./components/details-ventes/details-ventes.component";
import {NotifyComponent} from "./components/notify/notify.component";
import {PinchZoomModule} from 'ngx-pinch-zoom';

import { AngularFireModule } from '@angular/fire';
import { AngularFireDatabaseModule } from '@angular/fire/database';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import {environment} from '../environments/environment';
import {CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import firebase from 'firebase/app';
firebase.initializeApp(environment.firebase);
registerLocaleData(localeFr, 'fr');
import {HTTP} from "@ionic-native/http/ngx";

@NgModule({
    declarations: [
        AppComponent,
        SelectCountryComponent,
        NotifCenterComponent,
        NotifItemComponent,
        PanierComponent,
        PlatComponent,
        SignalerComponent,
        AddElementVenteComponent,
        DetailsVentesComponent,
        NotifyComponent
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA],
    entryComponents: [],
    imports: [BrowserModule, IonicModule.forRoot(), AppRoutingModule,
        FormsModule,
        ReactiveFormsModule,
        AngularFireModule.initializeApp(environment.firebase),
        AngularFireDatabaseModule,
        AngularFirestoreModule,
        HttpClientModule,
        PinchZoomModule,
    ],
    providers: [{provide: RouteReuseStrategy, useClass: IonicRouteStrategy},
        CountriesService,
        DatePipe,
        Geolocation,
        AndroidPermissions,
        LocationAccuracy,
        LaunchNavigator,
        SMS,
        Camera,
        File,
        NativeGeocoder,
        PinchZoomModule,
        HTTP,
        {provide: LOCALE_ID, useValue: "fr-FR"}
    ],
    bootstrap: [AppComponent],
    exports: [
    ]
})
export class AppModule {
}
