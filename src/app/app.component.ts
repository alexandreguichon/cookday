import {Component, NgZone} from '@angular/core';
import {CountriesService} from "./services/countries.service";
import {UserService} from "./services/user.service";
import {VentesService} from "./services/ventes.service";
import {LoadingController, Platform} from "@ionic/angular";
import {AngularFireAuth} from "@angular/fire/auth";
import { Router } from '@angular/router';
import { URLOpenListenerEvent, App } from '@capacitor/app';
import {FcmService} from "./services/fcm.service";

@Component({
    selector: 'app-root',
    templateUrl: 'app.component.html',
    styleUrls: ['app.component.scss'],
})
export class AppComponent {
    constructor(
        public countriesService: CountriesService,
        private userService: UserService,
        private ventesService: VentesService,
        private loadingController: LoadingController,
        public angularFireAuth: AngularFireAuth,
        private platform: Platform,
        private zone: NgZone,
        private router: Router,
    ) {
        this.platform.ready().then(() => {
            this.initializeApp();
        });
    }

    initializeApp() {
        this.router.navigate(['login']).then(()=>{
            App.addListener('appUrlOpen', (event: URLOpenListenerEvent) => {
                this.zone.run(() => {
                    // Example url: https://beerswift.app/tabs/tab2
                    // slug = /tabs/tab2
                    const slug = event.url.split(".app").pop();
                    if (slug) {
                        this.router.navigateByUrl(slug);
                    }
                    // If no match, do nothing - let regular routing
                    // logic take over
                });
            });
            let self = this;
            setTimeout(function () {
                if(!self.router.url.startsWith("/stripe-ok/")){
                    self.presentLoading("Vérification du statut de connexion").then(() => {
                        Promise.all([
                            self.userService.getMyLocation(),
                            self.countriesService.getCountryList(),
                            self.ventesService.getData(),
                        ]).then(() => {
                            self.login()
                            //this.loadingController.dismiss();
                        });
                    })
                }
            }, 500);
        })
    }

    async login() {
        return this.angularFireAuth.onAuthStateChanged(user => {
            if (user && user.emailVerified) {
                this.loadingController.dismiss().then(()=>{
                    if(!this.userService.user){
                        this.presentLoading("Connexion en cours").then(() => {
                            this.userService.userInfo = user;
                            return this.userService.getData();
                        });
                    }
                })
            } else {
                this.router.navigate(['principale']);
                return this.loadingController.dismiss();
            }
        });
    }

    async presentLoading(message) {
        const loading = await this.loadingController.create({
            message: message
        });
        await loading.present();
    }
}
