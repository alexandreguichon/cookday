import {Component, OnInit} from '@angular/core';
import {VentesService} from "../../services/ventes.service";
import {AlertController, PopoverController} from "@ionic/angular";
import {UserService} from "../../services/user.service";
import {Router} from "@angular/router";

@Component({
    selector: 'app-panier',
    templateUrl: './panier.component.html',
    styleUrls: ['./panier.component.scss'],
})
export class PanierComponent implements OnInit {

    constructor(
        public ventesService: VentesService,
        private alertController: AlertController,
        private popoverController: PopoverController,
        public userService: UserService,
        private router: Router
    ) {
    }

    ngOnInit() {
    }

    couleurCouverts() {
        return this.userService.user.panier.ajoutCouvertsServiettes ? 'var(--ion-color-customthird)' : 'var(--ion-color-medium)'
    }

    platMoins(plat) {
        if (plat.quantite > 1) {
            plat.quantite--;
            this.userService.updatePanier();
        } else {
            this.alertDeletePlat(plat);
        }
    }

    async alertDeletePlat(plat) {
        const alert = await this.alertController.create({
            header: "Retirer le plat",
            message: "Êtes vous sûr de vouloir retirer " + plat.nom + " du panier?",
            buttons: [
                {
                    text: "Annuler",
                    role: 'cancel',
                    cssClass: 'color-medium',
                    handler: (blah) => {
                    }
                },
                {
                    text: "Retirer",
                    cssClass: 'color-danger',
                    handler: (blah) => {
                        this.ventesService.retirerPlat(plat);
                    }
                },
            ]
        });
        await alert.present();
    }

    platPlus(plat) {
        if(this.qteReste(plat)){
            plat.quantite++;
            this.userService.updatePanier();
        }
    }

    qteReste(plat){
        let temp = Object.values(this.ventesService.ventes[this.userService.user.panier.vente].menu) as any[];
        let res: any;
        temp.forEach(cat => {
            let temp2 = cat.find(p => p.nom == plat.nom && p.prix == plat.prix);
            if(temp2) res = temp2;
        });
        return (res.quantite-(res.qteAchetee+res.quantiteAAjouter+plat.quantite))>0;
    }

    maxOpacity(plat){
        return this.qteReste(plat) ? '1' : '0.4';
    }

    openCuisine() {
        this.ventesService.open('cuisine', this.userService.user.panier.cuisine).then(() => {
            this.popoverController.dismiss();
        });
    }

    openChat(){
        this.ventesService.openChat(this.userService.user.panier.cuisine, this.userService.userInfo.uid).then(() => {
            this.popoverController.dismiss();
        });
    }

    isToday() {
        var todayDate = new Date();
        return this.ventesService.ventes[this.userService.user.panier.vente].date.getFullYear() == todayDate.getFullYear()
            && this.ventesService.ventes[this.userService.user.panier.vente].date.getMonth() == todayDate.getMonth()
            && this.ventesService.ventes[this.userService.user.panier.vente].date.getDate() == todayDate.getDate();
    }

    isTomorrow() {
        var todayDate = new Date();
        return this.ventesService.ventes[this.userService.user.panier.vente].date.getFullYear() == todayDate.getFullYear()
            && this.ventesService.ventes[this.userService.user.panier.vente].date.getMonth() == todayDate.getMonth()
            && this.ventesService.ventes[this.userService.user.panier.vente].date.getDate() == todayDate.getDate() + 1;
    }

    openFinaliserCommande(){
        this.router.navigate(['finaliser-commande']);
        this.popoverController.dismiss();
    }

    save(){
        this.userService.updatePanier();
    }

}
