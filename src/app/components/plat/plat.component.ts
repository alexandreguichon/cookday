import {Component, Input, OnInit} from '@angular/core';
import {Platform, PopoverController} from "@ionic/angular";
import {VentesService} from "../../services/ventes.service";
import {NavigationExtras, Router} from "@angular/router";
import {UserService} from "../../services/user.service";

@Component({
  selector: 'app-plat',
  templateUrl: './plat.component.html',
  styleUrls: ['./plat.component.scss'],
})
export class PlatComponent implements OnInit {

  @Input() plat: any;

  constructor(
      private popoverController: PopoverController,
      public ventesService: VentesService,
      public platform: Platform,
      private router: Router,
      public userService: UserService
  ) { }

  ngOnInit() {}

  close(){
    this.popoverController.dismiss();
  }

  openPhoto() {
    let navigationExtras: NavigationExtras = {
      queryParams: {
        img: JSON.stringify(this.plat.img)
      }
    };
    this.router.navigate(['photo-details'], navigationExtras).then(()=>{
      this.close();
    });
  }
}
