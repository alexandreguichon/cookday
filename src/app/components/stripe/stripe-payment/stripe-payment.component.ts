import {Component, Input, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {StripePaymentElementComponent, StripeService} from "ngx-stripe";
import {StripeHttpService} from "../../../services/stripe-http.service";
import {StripeElementsOptions} from "@stripe/stripe-js";
import {filter, map, share, tap} from "rxjs/operators";
import {merge} from "rxjs";
import {UserService} from "../../../services/user.service";
import {VentesService} from "../../../services/ventes.service";
import {AngularFirestore} from "@angular/fire/firestore";
import {NavController} from "@ionic/angular";
import firebase from 'firebase/app';
import {FcmService} from "../../../services/fcm.service";

@Component({
    selector: 'app-stripe-payment',
    templateUrl: './stripe-payment.component.html'
})
export class StripePaymentComponent implements OnInit, OnDestroy {

    @Input()
    public amount: number;

    @Input()
    public accountId: string;

    @Input()
    public heureRecup: any;

    @ViewChild(StripePaymentElementComponent)
    public paymentElement: StripePaymentElementComponent;

    public elementsOptions: StripeElementsOptions = {
        locale: 'fr'
    };

    paying = false;

    payed = false;

    paymentIntentId;

    constructor(private readonly stripeHttpService: StripeHttpService,
                private stripeService: StripeService,
                public userService: UserService,
                public ventesService: VentesService,
                public angularFirestore: AngularFirestore,
                private navController: NavController,
                private fcmService: FcmService,
    ) {
    }

    ngOnInit() {
        this.stripeHttpService.createPaymentIntentWithTransfer(this.amount, this.accountId).pipe(
            tap((paymentIntent) => this.paymentIntentId = paymentIntent.id),
            map((paymentIntent) => paymentIntent.client_secret),
            tap((clientSecret) => this.elementsOptions.clientSecret = clientSecret)
        ).subscribe();
    }

    ngOnDestroy() {
        if (this.payed || !this.paymentIntentId) {
            return;
        }
        this.stripeHttpService.cancelPaymentIntent(this.paymentIntentId).subscribe();
    }

    pay(): void {
        this.userService.presentLoading("Vérification du paiement...").then(() => {
            if (this.paying) {
                return;
            }
            this.paying = true;
            const paymentConfirm$ = this.stripeService.confirmPayment({
                elements: this.paymentElement.elements,
                confirmParams: {
                    payment_method_data: {
                        billing_details: {
                            name: this.userService.user.prenom + ' ' + this.userService.user.nom
                        }
                    }
                },
                redirect: 'if_required'
            }).pipe(
                share(),
                tap(() => this.paying = false)
            );
            merge(
                paymentConfirm$.pipe(
                    filter((result) => !!result.error),
                    tap((error) => {
                        console.log('Une erreur est survenue pendant le paiement... ', JSON.stringify(error));
                        alert('Une erreur est survenue pendant le paiement... ');
                        this.userService.loadingController.dismiss();
                    })
                ),
                paymentConfirm$.pipe(
                    filter((result) => !result.error),
                    filter((result) => result.paymentIntent.status === 'succeeded'),
                    tap(() => {
                        this.userService.loadingController.dismiss().then(() => this.finaliserCommande());
                        /** Le paiement a fonctionné, faire une redirection */
                    })
                )
            ).subscribe();
        });
    }

    finaliserCommande() {
        this.userService.presentLoading("Commande en cours").then(() => {
            let dateRecup = new Date(this.ventesService.ventes[this.userService.user.panier.vente].date);
            let hours = new Date(this.heureRecup);
            dateRecup.setHours(hours.getHours(), hours.getMinutes());
            let newCommande = {
                ajoutCouvertsServiettes: this.userService.user.panier.ajoutCouvertsServiettes,
                cuisine: this.userService.user.panier.cuisine,
                dateRecup: new Date(dateRecup),
                dateRes: new Date(),
                menu: this.userService.user.panier.menu,
                statut: "à préparer",
                user: this.userService.userInfo.uid,
                vente: this.userService.user.panier.vente,
            }

            this.angularFirestore.collection("commandes").add(newCommande).then(res => {
                this.angularFirestore.collection("commandes").doc(res.id).update({id: res.id}).then(() => {
                    this.userService.user.commandes.push(res.id);
                    newCommande['id'] = res.id;
                    this.userService.commandesData[res.id] = newCommande;

                    this.ventesService.order.forEach(cat => {
                        this.userService.user.panier.menu[this.ventesService.retirerAccents(cat)].forEach(plat => {
                            this.ventesService.ventes[this.userService.user.panier.vente].menu[this.ventesService.retirerAccents(cat)].find(e => e.nom == plat.nom && e.prix == plat.prix).qteAchetee += plat.quantite;
                        })
                    })

                    this.ventesService.ventes[this.userService.user.panier.vente].commandes.push(res.id);
                    Promise.all([
                        this.angularFirestore.collection("ventes").doc(this.userService.user.panier.vente).update({
                            commandes: this.ventesService.ventes[this.userService.user.panier.vente].commandes
                        }),
                        this.angularFirestore.collection("utilisateurs").doc(this.userService.userInfo.uid).update({
                            commandes: this.userService.user.commandes,
                            panier: null
                        }),
                        this.angularFirestore.collection('utilisateurs').doc(this.userService.user.panier.cuisine).update({
                            notifs: firebase.firestore.FieldValue.arrayUnion({
                                cuisine: this.userService.user.panier.cuisine,
                                user: this.userService.userInfo.uid,
                                date: new Date(),
                                text: this.userService.userInfo.prenom + " a passé une commande.",
                                action: "commandeRecue",
                                objectId: res.id
                            })
                        }),
                        this.fcmService.sendPostRequest(
                            'Nouvelle commande',
                            this.userService.userInfo.prenom + " a passé une commande.",
                            this.userService.user.photo ? this.userService.user.photo : "assets/addPhoto.jpg",
                            newCommande,
                            "",
                            this.userService.userInfo.uid,
                            this.ventesService.getCuisineById(this.userService.user.panier.cuisine).token,
                            this.userService.userInfo.prenom + " a passé une commande.",
                            "commandeRecue",
                            "")
                    ]).then(() => {
                        this.back().then(() => {
                            this.userService.user.panier = null;
                            this.userService.loadingController.dismiss();
                            this.userService.toast("La commande a été passée.", 2000);
                        });
                    })
                })
            })
        })
    }

    async back() {
        return this.navController.pop();
    }

}
