import {Component, Input, OnInit} from '@angular/core';
import {ModalController} from "@ionic/angular";
import {UserService} from "../../services/user.service";
import {VentesService} from "../../services/ventes.service";
import {NavigationExtras, Router} from "@angular/router";

@Component({
    selector: 'app-notify',
    templateUrl: './notify.component.html',
    styleUrls: ['./notify.component.scss'],
})
export class NotifyComponent implements OnInit {

    showBackdrop = false;

    // Data passed in by componentProps
    @Input() notif: {
        action: string;
        cuisine: string;
        date: string;
        objectId: string;
        text: string;
        user: string;
    };

    constructor(
        public modalController: ModalController,
        private userService: UserService,
        private ventesService: VentesService,
        private router: Router
    ) {
    }

    ngOnInit() {
        let self = this;
        setTimeout(function () {
            self.modalController.dismiss();
        }, 2000);
    }

    onClick() {
        this.userService.setNotifLue(this.notif);
        switch (this.notif.action) {
            case "message":
                this.ventesService.openChat(this.notif.cuisine, this.notif.user);
                break;
            case "commande":
                let navigationExtras: NavigationExtras = {
                    queryParams: {
                        slideTo: JSON.stringify("commandes"),
                    }
                };
                this.router.navigate(['principale'], navigationExtras)
                break;
            default:
                break;
        }
        this.userService.save();
        this.modalController.dismiss();
    }

    getNom(){
        return this.userService.userInfo.uid==this.notif.cuisine ? this.userService.getUserById(this.notif.user).prenom : this.ventesService.getCuisineById(this.notif.cuisine).nom;
    }

    getImage(){
        return this.userService.userInfo.uid==this.notif.cuisine ? this.userService.getUserById(this.notif.user).photo : this.ventesService.getCuisineById(this.notif.cuisine).photos[0];
    }

}
