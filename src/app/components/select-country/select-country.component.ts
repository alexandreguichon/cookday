import {Component, OnInit} from '@angular/core';
import {PopoverController} from "@ionic/angular";
import {UserService} from "../../services/user.service";
import {CountriesService} from "../../services/countries.service";

@Component({
    selector: 'app-select-country',
    templateUrl: './select-country.component.html',
    styleUrls: ['./select-country.component.scss'],
})
export class SelectCountryComponent implements OnInit {

    constructor(
        private popoverController: PopoverController,
        public countriesService: CountriesService
    ) {
    }

    ngOnInit() {
    }

    onCountryClick(country: any) {
        country.flag = "https://cdn.countryflags.com/thumbs/" + country.name.toLowerCase() + "/flag-3d-round-250.png";
        this.countriesService.selectedCountry = country;
        this.popoverController.dismiss();
    }

}