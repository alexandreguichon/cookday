import {Component, Input, OnInit} from '@angular/core';
import {VentesService} from "../../services/ventes.service";

@Component({
  selector: 'app-details-ventes',
  templateUrl: './details-ventes.component.html',
  styleUrls: ['./details-ventes.component.scss'],
})
export class DetailsVentesComponent implements OnInit {

  @Input() vente: any;

  constructor(
      public ventesService: VentesService
  ) { }

  ngOnInit() {}

}
