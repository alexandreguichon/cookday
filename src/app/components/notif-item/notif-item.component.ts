import {Component, Input, OnInit} from '@angular/core';
import {VentesService} from "../../services/ventes.service";
import {UserService} from "../../services/user.service";
import {PopoverController} from "@ionic/angular";
import {NavigationExtras, Router} from "@angular/router";

@Component({
    selector: 'app-notif-item',
    templateUrl: './notif-item.component.html',
    styleUrls: ['./notif-item.component.scss'],
})
export class NotifItemComponent implements OnInit {

    @Input() notif: any;

    constructor(
        private ventesService: VentesService,
        public userService: UserService,
        private popoverController: PopoverController,
        private router: Router
    ) {
    }

    ngOnInit() {
    }

    onClick() {
        this.popoverController.dismiss();
        this.userService.setNotifLue(this.notif);
        switch (this.notif.action) {
            case "message":
                this.ventesService.openChat(this.notif.cuisine, this.notif.user);
                break;
            case "commande":
                this.router.navigate(['principale'], {
                    queryParams: {
                        slideTo: JSON.stringify("commandes"),
                    }
                })
                break;
            case "commandeRecue":
                this.router.navigate(['principale'], {
                    queryParams: {
                        slideTo: JSON.stringify("cuisine"),
                    }
                })
                break;
            default:
                break;
        }
        this.userService.save();
    }

    getCuisine() {
        return this.ventesService.getCuisineById(this.notif.cuisine);
    }

    getUser() {
        return this.userService.getUserById(this.notif.user);
    }

    getDateToAffich() {
        var date = this.notif.date.toDate();
        let now = new Date();
        var minutesAgo = Math.round((now.getTime() - date.getTime()) / 60000);
        if (minutesAgo < 60) {
            return minutesAgo + "m";
        } else {
            var hoursAgo = Math.round(minutesAgo / 60);
            if (hoursAgo < 24) {
                return hoursAgo + "h";
            } else {
                var daysAgo = Math.round(hoursAgo / 24);
                if (daysAgo < 30) {
                    return daysAgo + "j";
                } else {
                    var monthsAgo = Math.round(daysAgo / 30);
                    if (monthsAgo < 12) {
                        return monthsAgo + "M";
                    } else {
                        return Math.round(monthsAgo / 12) + "a";
                    }
                }
            }
        }
    }

    getBackgroundColor() {
        return this.notif.lu ? '#FFFFFF' : '#D2EDFF';
    }

}
