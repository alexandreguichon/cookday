import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {UserService} from "../../services/user.service";
import {VentesService} from "../../services/ventes.service";
import {NavigationExtras, Router} from "@angular/router";
import {CapacitorGoogleMaps} from "@capacitor-community/google-maps";

@Component({
    selector: 'app-map',
    templateUrl: './map.component.html',
    styleUrls: ['./map.component.scss'],
})
export class MapComponent implements OnInit {

    @ViewChild('map') mapView: ElementRef;

    constructor(
        private userService: UserService,
        private ventesService: VentesService,
        private router: Router
    ) {
    }

    ngOnInit() {
    //     setTimeout(() => {
    //         console.log("test");
    //         this.createMap()
    //     }, 200);
    }
    //
    // ionViewDidEnter() {
    //
    // }
    //
    // createMap() {
    //     const boundingRect = this.mapView.nativeElement.getBoundingClientRect() as DOMRect;
    //
    //     CapacitorGoogleMaps.create({
    //         width: Math.round(boundingRect.width),
    //         height: Math.round(boundingRect.height),
    //         x: Math.round(boundingRect.x),
    //         y: Math.round(boundingRect.y),
    //         zoom: 12,
    //         latitude: this.userService.userlat,
    //         longitude: this.userService.userlng
    //     });
    //
    //     CapacitorGoogleMaps.addListener('onMapReady', async () => {
    //         CapacitorGoogleMaps.setMapType({
    //             type: "normal" // hybrid, normal, satellite, terrain
    //         });
    //
    //         this.showCurrentPosition()
    //             .then(() => this.showCuisines());
    //
    //         CapacitorGoogleMaps.setOnMarkerClickListener().then((res) => {
    //             CapacitorGoogleMaps.addListener('didTap', async (ev) => {
    //                 this.openCuisine(ev.metadata.id);
    //             });
    //         })
    //     });
    //
    // }
    //
    // async showCurrentPosition() {
    //     // Create our current location marker
    //     CapacitorGoogleMaps.addMarker({
    //         latitude: this.userService.userlat,
    //         longitude: this.userService.userlng,
    //     });
    //
    //     // Focus the camera
    //     return CapacitorGoogleMaps.setCamera({
    //         latitude: this.userService.userlat,
    //         longitude: this.userService.userlng,
    //         zoom: 13,
    //         bearing: 0
    //     });
    // }
    //
    // async showCuisines() {
    //     let res = Object.values(this.ventesService.cuisines) as any[];
    //     res.forEach(cuisine => {
    //         CapacitorGoogleMaps.addMarker({
    //             latitude: cuisine.geoloc.latitude,
    //             longitude: cuisine.geoloc.longitude,
    //             iconUrl: 'https://firebasestorage.googleapis.com/v0/b/cookday-9366d.appspot.com/o/Icones%20marqueurs%2FMarqueur_Cookday_proposition2%2050px.png?alt=media&token=85830fce-6a86-4264-a39f-f9a5a72ceb9e',
    //             title: cuisine.nom,
    //             snippet: cuisine.adresse,
    //             metadata: {id: cuisine.id}
    //         } as any).then(res => {
    //         })
    //     });
    //     return true;
    // }
    //
    //
    // ionViewWillLeave() {
    //     CapacitorGoogleMaps.hide();
    // }
    //
    // openCuisine(id) {
    //     this.ventesService.open('cuisine', id);
    //     let navigationExtras: NavigationExtras = {
    //         replaceUrl: true,
    //         queryParams: {
    //             id: JSON.stringify(id)
    //         }
    //     };
    //     return this.router.navigate(['cuisine'], navigationExtras);
    // }

}
