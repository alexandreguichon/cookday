import {Component, Input, OnInit} from '@angular/core';
import {PopoverController} from "@ionic/angular";
import {VentesService} from "../../services/ventes.service";
import {UserService} from "../../services/user.service";

@Component({
    selector: 'app-signaler',
    templateUrl: './signaler.component.html',
    styleUrls: ['./signaler.component.scss'],
})
export class SignalerComponent implements OnInit {

    @Input() id: any;
    motif: string = "";
    motifDescription: string = "";

    constructor(
        private popoverController: PopoverController,
        private ventesService: VentesService,
        private userService: UserService
    ) {
    }

    ngOnInit() {
        console.log(this.id);
    }

    disabledBoutonConfirmer() {
        if (this.motif == "") {
            return true;
        } else {
            if (this.motif == "Autre" && this.motifDescription == "") {
                return true;
            }
        }
        return false;
    }

    signaler() {
        this.close();
        if (this.motif == "Autre") {
            //this.matchService.signalerParticipant(this.player, this.motifDescription);
        } else {
            //this.matchService.signalerParticipant(this.player, this.motif);
        }
    }

    close() {
        this.popoverController.dismiss();
    }

    cuisine(){
        return this.ventesService.cuisines[this.userService.user.commandes[this.id].cuisine];
    }
}
