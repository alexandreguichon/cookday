import {Component, OnInit} from '@angular/core';
import {UserService} from "../../services/user.service";
import {VentesService} from "../../services/ventes.service";
import {PopoverController} from "@ionic/angular";

@Component({
    selector: 'app-add-element-vente',
    templateUrl: './add-element-vente.component.html',
    styleUrls: ['./add-element-vente.component.scss'],
})
export class AddElementVenteComponent implements OnInit {

    constructor(
        public userService: UserService,
        public ventesService: VentesService,
        private popoverController: PopoverController
    ) {
    }

    ngOnInit() {
    }

    addPlat(cat: string, plat: any) {
        this.ventesService.newVente.menu[this.ventesService.retirerAccents(cat)].push({
            img: plat.img,
            ingredients: plat.ingredients,
            nom: plat.nom,
            prix: plat.prix,
            quantite: null,
            qteAchetee: 0
        });
        this.popoverController.dismiss();
    }

    dejaDansVente(cat: string, plat: any){
        return this.ventesService.newVente.menu[this.ventesService.retirerAccents(cat)].some(e => e.img==plat.img);
    }

}
