import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-notation',
  templateUrl: './notation.component.html',
  styleUrls: ['./notation.component.scss'],
})
export class NotationComponent implements OnInit {

  @Input() note: any;

  constructor() { }

  ngOnInit() {}

  truncate(val) {
    return val ? Math.trunc(this.roundHalf(val)) : 0;
  }

  isHalfRating(val) {
    return val ? this.truncate(this.roundHalf(val)) != this.roundHalf(val) : false;
  }

  roundHalf(num) {
    return Math.round(num * 2) / 2;
  }

  nbEmptyStars(val) {
    let res = 5 - this.truncate(val);
    return this.isHalfRating(val) ? res - 1 : res;
  }

}
