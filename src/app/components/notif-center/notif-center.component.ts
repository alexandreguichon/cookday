import {Component, OnInit} from '@angular/core';
import {AlertController, PopoverController} from "@ionic/angular";
import {UserService} from "../../services/user.service";

@Component({
    selector: 'app-notif-center',
    templateUrl: './notif-center.component.html',
    styleUrls: ['./notif-center.component.scss'],
})
export class NotifCenterComponent implements OnInit {


    constructor(
        private alertController: AlertController,
        private popoverController: PopoverController,
        public userService: UserService
    ) {
    }

    ngOnInit() {
    }

    async toutMarquerLu() {
        const alert = await this.alertController.create({
            header: "Tout marquer comme lu",
            message: "Toutes tes notifications vont être marquées comme lues. Tu confirmes ton action?",
            buttons: [
                {
                    text: 'Annuler',
                    role: 'cancel',
                    cssClass: 'secondary',
                    handler: (blah) => {
                    }
                },
                {
                    text: 'Confirmer',
                    handler: () => {
                        this.userService.setAllNotifsLues();
                    }
                }
            ]
        });

        await alert.present();
    }

    close() {
        this.popoverController.dismiss();
    }

    hasNotRiddenMessages(){
        return this.userService.user && this.userService.user.notifs && this.userService.user.notifs.some(e => !e.lu)
    }

}
