import {Component, Input, OnInit} from '@angular/core';
import {VentesService} from "../../services/ventes.service";
import {Platform} from "@ionic/angular";
import {UserService} from "../../services/user.service";

@Component({
    selector: 'app-vente-item',
    templateUrl: './vente-item.component.html',
    styleUrls: ['./vente-item.component.scss'],
})
export class VenteItemComponent implements OnInit {

    @Input() vente: any;

    constructor(
        public ventesService: VentesService,
        public platform: Platform,
        public userService: UserService
    ) {
    }

    ngOnInit() {
    }

    getNote(vente) {
        return this.ventesService.getCuisine(vente).avis.length == 0 ? "-" : this.ventesService.getMoyenne(this.ventesService.getCuisine(vente))
    }

    getNoteColor(vente) {
        let note = this.getNote(vente);
        if (note >= 4) {
            return "#98FF90";
        } else {
            if (note >= 2.5) {
                return "#FFFA90";
            } else {
                return "#FF9090";
            }
        }
    }

    getProfileImg() {
        return this.ventesService.getCuisine(this.vente).photos[0];
    }

}
