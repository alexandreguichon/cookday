import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'com.whalex.cookday',
  appName: 'CookDay',
  webDir: 'www',
  bundledWebRuntime: false,
  plugins: {
    CapacitorFirebaseAuth: {
      providers: ["google.com", "twitter.com", "facebook.com", "phone"],
      languageCode: "en",
      nativeAuth: false,
      properties: {
        google: {
          // hostedDomain: "my-custom-domain.com" // Supprimé pour éviter les restrictions
        }
      },
      permissions: {
        google: ["profile", "email"] // Permissions simplifiées
      }
    }
  }
};